(function() {

	// Navigator data
	navigator.data = {
		retryCount : 3,
		status : 'unknown'
	};

	// Helpers
	$.extend($, {
		// A helper that dispatch geo uri to GPS application
		openIntent : function(type, coor) {
			var url;
			if ($.isMobile) {
				switch (type) {
				case 'default':
					url = 'geo:' + coor.lat + ',' + coor.lng;
					break;
				case 'sygic':
					url = 'com.sygic.aura://coordinate|' + coor.lng + '|' + coor.lat + '|show';
					break;
				case 'sygic-old':
					url = 'http://com.sygic.aura/coordinate|' + coor.lng + '|' + coor.lat + '|show';
					break;
				default:
					url = '';
					break;
				}
				console.log(type + ' | ' + url);
				if (window.plugins && window.plugins.webintent) {
					window.plugins.webintent.startActivity({
						action : window.plugins.webintent.ACTION_VIEW,
						url : url
					}, function() {
					}, function(err) {
						if (type === 'sygic') {
							if (window.Config.maps.sygic) {
								$.openIntent('sygic-old', coor);
							} else {
								window.Notify.pop('Error, Sygic is not installed');
							}
						} else {
							window.Notify.pop('Error, No navigator installed');
						}
					});
				}
			} else {
				url = 'http://maps.google.com/?q=' + coor.lat + ',' + coor.lng;
				var win = window.open(url, '_blank');
				win.focus();
			}
		},
		getLocation : function(fnSuccess, fnError) {
			console.log('Getting Location | ' + navigator.onLine + " | " + navigator.data.status);
			if (navigator.data.status == 'getting') {
				return;
			}
			navigator.data.status = 'getting';
			navigator.geolocation.watchPosition(function(pos) {
				// console.log('Succes: ' + JSON.stringify(pos));
				Config.location = pos.coords;
				fnSuccess(Config.location);
			}, function(error) {
				// console.log('Failed: ' + JSON.stringify(error));
				switch (error.code) {
				case 1:
				case 2:
					// User denied Geolocation. Stop trying again.
					nextTick(function() {
						navigator.data.status = 'error';
						fnError(error);
					});
					break;
				case 3:
				default:
					// Try to get location again
					if (navigator.data.retryCount-- > 0) {
						window.setTimeout(function() {
							navigator.data.status = 'refresh';
							$.getLocation(fnSuccess, fnError);
						}, 1000 * 5);

					} else {
						nextTick(function() {
							navigator.data.status = 'error';
							fnError(error);
						});
					}
					break;
				}
			}, {
				timeout : 1000 * 5,
				enableHighAccuracy : false
			});
		}
	});

	// Configuration for Backbone app
	App.configure(Config.application);

})();
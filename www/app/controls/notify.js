App.View.Notify = Backbone.View.extend({
	el: '#gp-notify',
	delay: 2000,
	timeout: null,
	style: function(css, win) {
		css({
			'> div': {
				'border': win.pxMin(0.004) + ' solid #222',
			},
			'.gp-notify-text': {
				'padding': win.pxMin(0.015),
				'border': win.pxMin(0.002) + ' solid #555',
				'max-width': win.pxMin(0.8),
				'font-size': win.pxMin(0.035),
			}
		});
	},
	init: function() {
		this.callbackHide = $.proxy(this.callbackHide, this);
	},
	pop: function(msg, delay) {
		console.log(msg);
		// Replace msg
		this.$el.find('.gp-notify-text').html(msg);
		// Show the view
		this.show();
		// Neet to clear previous timeout
		clearTimeout(this.timeout);
		// Create new time out
		this.timeout = setTimeout(this.callbackHide, delay || this.delay)

	},
	callbackHide: function() {
		this.hide();
	},
	show: function() {
		var self = this, args = arguments;
		this.$el.fadeIn(300, function() {
			self.constructor.__super__.show.apply(self, args);
		});
	},
	hide: function() {
		var self = this, args = arguments;
		this.$el.fadeOut(300, function() {
			self.constructor.__super__.hide.apply(self, args);
		});
	}
});
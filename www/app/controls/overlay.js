/**
 * touchstart event is not fully supported by most mobile browser
 */

App.View.Overlay = Backbone.View.extend({
	el : '#gp-overlay',
	controlBack : true,
	views : [],
	events : {
	// 'touchstart' : 'touchStartBackground'
	},
	init : function() {
		// Init
	},
	done : function() {
		// Done
	},
	show : function(view) {
		// Replace content
		if (view) {

			var ndx = _.indexOf(this.views, view);

			_.each(this.views, function(val) {
				val.hide();
			});

			if (ndx === -1) {
				this.views.push(view);
				this.placeInChild(view, '.app-overlay-container');
			} else {
				this.views[ndx].show();
			}

		}
		this.super.show.call(this);
		// view.delegateEvents();
	},
	touchStartBackground : function(e) {
		// e.preventDefault();
		return;
	}
});
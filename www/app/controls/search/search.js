/**
 * 
 * Search In Nearby Places
 * 
 * https://maps.googleapis.com/maps/api/place/nearbysearch/json
 * ?key=AIzaSyA4Id0T98QsKJ6H-TXGBqHeE9x0bNHPse8&rankby=distance
 * &location=-33.8670522,151.1957362&keyword=cruise
 * 
 * Search In Global Places
 * 
 * https://maps.googleapis.com/maps/api/place/textsearch/json
 * ?key=AIzaSyA4Id0T98QsKJ6H-TXGBqHeE9x0bNHPse8&sensor=false&query=cruise
 * 
 */

App.View.Search = Backbone.View.extend({
	/**
	 * @memberOf App.View.Search
	 */
	id : 'gp-search',
	reqQuery : '',
	reqCount : 1,
	// reqUrl : 'http://gamexplode.com/proxy/googleplaces',
	reqUrl : 'https://maps.googleapis.com/maps/api/place/textsearch/json',
	reqUrlNearBy : 'https://maps.googleapis.com/maps/api/place/nearbysearch/json',
	reqKey : 'AIzaSyA4Id0T98QsKJ6H-TXGBqHeE9x0bNHPse8',
	reqSensor : false,
	hisItemCount : 5,
	keys : [ 'formatted_address', 'geometry', 'name', 'id' ],
	cache : localStorage || {},
	cacheLimit : 10,
	cacheStack : null,
	_loading : false,
	_count : 0,
	events : {
		'click .gp-error-query' : 'clickError',
		'click .gp-search-history a' : 'clickHistory',
		'click .gp-main-logo' : 'clickLogo'
	},

	style : function(css, win) {
		var self = this, fontSize = 0.045, textPadding = win.ptMin(0.035), $inputText = this.$el
				.find('input[type=text]');
		css({
			'' : {
				'font-size' : win.ptMin(fontSize) + 'px'
			},
			'.gp-search-form input[type=text]' : {
				'font-size' : win.pxMin(fontSize),
				'padding' : textPadding + 'px'
			},
			'.gp-search-form input[type=submit]' : {
				'font-size' : win.pxMin(fontSize),
				'padding' : textPadding + 'px'
			},
			'.gp-main-logo' : {
				'margin-top' : '-' + win.pxMin(0.1)
			},
			'.gp-main-logo img' : {
				'width' : win.pxMin(0.4)
			},
			'.gp-center-logo' : {
				'margin-bottom' : win.pxMin(0.05)
			},
			'.gp-center-logo img' : {
				'height' : win.pxMin(0.13)
			},
			'.gp-loading img' : {
				'width' : win.pxMin(0.08)
			},
			'.gp-error-limit img' : {
				'width' : win.pxMin(0.25),
				'margin-bottom' : win.pxMin(0.05)
			},
			'.gp-error-query img, .gp-info img' : {
				'width' : win.pxMin(0.3),
				'margin-bottom' : win.pxMin(0.05)
			},
			'.gp-error h4, .gp-info h4' : {
				'margin-bottom' : win.pxMin(0.03)
			},
			'.gp-search-history' : {
				'border-bottom' : win.pxMin(0.004) + ' solid #3a3a3a'
			},
			'.gp-search-history a' : {
				'padding' : win.pxMin(0.03),
				'border-top' : win.pxMin(0.002) + ' solid #c1c1c1'
			}
		});

		async.nextTick(function() {
			css({
				'input[type=text]' : {
					'width' : '100%'
				},
				'.gp-search-form-submit' : {
					'width' : self.$el.find('input[type=submit]').outerWidth(true) + 'px'
				},
				'.gp-search-content' : {
					'margin-top' : ($inputText.outerHeight(true)) + 'px'
				}
			});
		});

	},
	init : function() {
		this.result = new App.View.SearchResult();
		this.favorites = new App.View.Favorites();
		this.cacheStack = _.keys(this.cache);
	},
	done : function() {

		var self = this;

		this.prepend(this.favorites, '.gp-search-content');

		this.$result = this.$el.find('.gp-search-result');
		this.prepend(this.result, '.gp-search-result');
		this.result.$el.empty();
		this.logo = this.$el.find('.gp-main-logo');

		this.$spin = this.$el.find('.gp-loading');
		this.$info = this.$el.find('.gp-info');
		this.$errorQuery = this.$el.find('.gp-error-query');
		this.$errorLimit = this.$el.find('.gp-error-limit');
		this.$input = this.$el.find('.gp-search-form-text input');
		this.$history = this.$el.find('.gp-search-history');

		this.$el.find('.gp-search-form').submit(function(e) {
			e.preventDefault();
			var input = $(this).find('input[name=query]'), query = $.trim(input.val());
			if (query.length === 0)
				return;
			self.reqQuery = query;
			self._count = self.reqCount;
			self.request(query);
			input.blur();
			if (_.indexOf(window.Config.searchHistory, query) === -1) {
				window.Config.searchHistory.unshift(query);
			}
		});

		this.$input.on('input', function(e) {
			self.showHistory(self.$input.val());
		}).on('blur', function() {
			setTimeout(function() {
				self.$history.hide();
			}, 500);
		}).on('click', function() {
			self.$history.hide();
		});

		document.addEventListener('hidekeyboard', function() {
			self.$history.hide();
		});

		this.result.on('hide', function() {
			self.$result.hide();
		});

		// Enable css button click animation
		this.$el.button('input[type=submit]');
		this.$el.find('.gp-search-history').button('a');
		this.favorites.$el.show().css('visibility', 'visible');

		var half = ($(window).height() / 1.2);
		$(window).on('resize', function() {
			if (half > $(this).height()) {
				window.Body.search.logo.hide();
			} else {
				if (_.isEmpty(window.Config.favorites)) {
					setTimeout(function() {
						window.Body.search.logo.show();
					}, 0);
				}
			}
		});
		this.getLocation();
	},
	show : function() {
		// Re compile cache stack when we show this view,
		// as local storage might be updated dynamically
		this.cacheStack = _.keys(this.cache);
		this.result.hide();
		this.super.show.call(this);
		this.getLocation();

	},
	getLocation : function() {
		// Checking coverage option
		var self = this;
		if (Config.app.coverage == 'nearby') {
			if (Config.location == null) {
				// console.log('Getting location');
				self.$input.attr('placeholder', 'Global Search');
				setTimeout(function() {
					Head.showStatus('Obtaining your location...', 'info');
					$.getLocation(function() {
						// Success
						Head.hideStatus('Location obtained', 'success');
						self.$input.attr('placeholder', 'Nearby Search');
					}, function() {
						// Error
						Head.hideStatus('Unable to obtain your location', 'error');
						// self.$input.attr('placeholder', 'Global Search');
						// setTimeout(function(){
						// Head.hideStatus();
						// }, 4000);
					});
				}, 500);
			} else {
				self.$input.attr('placeholder', 'Nearby Search');
			}
		} else {
			self.$input.attr('placeholder', 'Global Search');
		}
	},
	request : function() {

		if (this._loading)
			return;
		var self = this, query = this.reqQuery.toLowerCase();

		this.$result.show();
		this.$result.children().hide();

		this.error(false);
		this.loading(true);

		// Check cache
		if (this.cache[query]) {
			self.loading(false);
			self.response(self.cacheGet(query), true);
		} else {
			// Request new data
			// console.log('Requesting... ' + this.reqUrl + " | ");
			// +this.reqQuery);

			var url, params = {
				key : this.reqKey,
				sensor : this.reqSensor
			};

			if (Config.app.coverage == 'nearby' && Config.location != null && Config.location.latitude
					&& Config.location.longitude) {
				url = this.reqUrlNearBy;
				params.rankby = 'distance';
				params.location = Config.location.latitude + ',' + Config.location.longitude;
				params.keyword = query;
			} else {
				url = this.reqUrl;
				params.query = query;
			}
			console.log(url, params);
			$.ajax(url, {
				data : params,
				complete : function(data, status) {
					// console.log('Response... ' + JSON.stringify(data)
					// + " | " + status);
					// console.log('Arguments... ' +
					// JSON.stringify(arguments));
					self.loading(false);
					self.response(data, false);
				}
			});
		}

	},
	response : function(data, cache) {
		var status = null;

		if (cache) {
			// If from cache, status should always be OK
			status = 'OK';
		} else if (data.responseJSON && data.responseJSON.status) {
			status = data.responseJSON.status;
		}

		this.$info.hide();
		this.$errorQuery.hide();
		this.$errorLimit.hide();

		// status = 'OVER_QUERY_LIMIT';

		switch (status) {
		case 'OK':
			var self = this, results = cache ? data : data.responseJSON.results;
			this.result.$el.empty();
			if (cache) {
				$.each(results, function(i, val) {
					self.result.addResultItem(val);
				});
			} else {
				results = _.map(results, function(val) {
					self.result.addResultItem(val);
					return _.pick(val, self.keys);
				});
				self.cacheIn(self.reqQuery.toLowerCase(), results);
			}
			this.result.show();
			break;
		case 'ZERO_RESULTS':
			this.info(true, 'no result found');
			break;
		case 'OVER_QUERY_LIMIT':
			this.errorLimit(true);
			break;
		default:
			if (--this._count) {
				this.request();
			} else {
				this.error(true, 'tap here to retry');
			}
		}
	},
	cacheIn : function(name, data) {
		// Check if cache settings is enabled or limit is set
		if (!window.Config.app.cacheQuery || this.cacheLimit <= 0)
			return;
		if (this.cacheStack.length >= this.cacheLimit) {
			var self = this, omit = _.rest(this.cacheStack, this.cacheLimit - 1);
			_.each(omit, function(val) {
				delete self.cache[val];
				self.cacheStack.splice(_.indexOf(this.cacheStack, val), 1);
			});
		}
		if (_.indexOf(this.cacheStack, name) === -1) {
			this.cacheStack.unshift(name);
			this.cache[name] = JSON.stringify(data);
		}
	},
	cacheGet : function(name) {
		return JSON.parse(this.cache[name]);
	},
	info : function(show, message) {
		if (show) {
			this.result.$el.empty();
			this.$info.show();
			this.$info.find('.gp-text').text(message);
		} else {
			this.$info.hide();
		}
	},
	error : function(show, message) {
		if (show) {
			this.result.$el.empty();
			this.$errorQuery.show();
			this.$errorQuery.find('.gp-error-text').text(message);
		} else {
			this.$errorQuery.hide();
		}
	},
	errorLimit : function(show) {
		if (show) {
			this.result.$el.empty();
			this.$errorLimit.show();
		} else {
			this.$errorLimit.hide();
		}
	},
	loading : function(show) {
		if (show) {
			this.result.$el.empty();
			this.$spin.show();
		} else {
			this.$spin.hide();
		}
		this._loading = show;
	},
	clickError : function(e) {
		this._count = this.reqCount;
		this.request();
	},
	showHistory : function(input) {
		if (input.length === 0) {
			this.$history.hide();
			return;
		}
		this.$history.empty().show();
		var i = 0, count = 0, history = window.Config.searchHistory;

		for (; i < history.length; i++) {
			if (history[i].toLowerCase().indexOf(input.toLowerCase()) > -1 && count < this.hisItemCount) {
				this.$history.append('<li><a>' + history[i] + '</a></li>');
				count++;
			}
		}
	},
	clickHistory : function(e) {
		this.$input.val($(e.target).text());
		this.$history.hide();
	},
	clickLogo : function(e) {
		// console.log('Logo Clicked!');
		// $.getLocation(function() {
		// }, function() {
		// });
	}
});
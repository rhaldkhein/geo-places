App.View.FavAction = Backbone.View.extend({
	/**
	 * @memberOf App.View.FavAction
	 */
	className : 'gp-fav-action',
	control : null,
	events : {
		// 'click a' : 'clickAnchor',
		'click .el-close' : 'clickClose',
		'click .el-rename' : 'clickRename',
		'click .el-info' : 'clickInfo',
		'click .el-share' : 'clickShare',
		'click .el-delete' : 'clickDelete'
	},
	style : function(css, vp) {
		css({
			'' : {
				'width' : vp.pxMin(0.5),
				'font-size' : vp.pxMin(0.04)
			},
			'ul' : {
				'border' : vp.pxMin(0.005) + ' solid #1a1a1a'
			},
			'li a' : {
				'padding' : vp.pxMin(0.04),
				'border-top' : vp.pxMin(0.002) + ' solid #9a9a9a'
			},
			'div a' : {
				'padding' : vp.pxMin(0.04),
				'margin' : 'auto',
				'margin-top' : vp.pxMin(0.04),
				// 'width': vp.pxMin(0.3),
				'border' : vp.pxMin(0.002) + ' solid #1a1a1a'
			},
			'li:first-child a' : {
				'border-top' : 'none'
			}
		})
	},
	init : function(opt) {
		this.control = opt.control;
	},
	done : function() {
		this.$el.button('a');
	},
	clickClose : function() {
		this.parent.hide();
	},
	clickRename : function() {
		this.control.clickRename();
		this.parent.hide();
		// console.log('clickRename');
	},
	clickInfo : function() {
		this.control.clickInfo();
		this.parent.hide();
		// console.log('clickInfo');
	},
	clickShare : function() {
		this.control.clickShare();
		this.parent.hide();
		// console.log('clickShare');
	},
	clickDelete : function() {
		this.control.clickDelete();
		this.parent.hide();
		// console.log('clickDelete ' + window.Body.search.favorites);
	},
	clickAnchor : function() {
		this.parent.hide();
	}
});
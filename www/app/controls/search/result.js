App.View.SearchResult = Backbone.View
		.extend({
			/**
			 * @memberOf App.View.SearchResult
			 */
			tagName : 'ul',
			className : 'gp-result-result-wrap',
			controlBack : true,
			events : {
				'click .gp-result-item a' : 'clickItem',
				'click .gp-result-item > span' : 'clickAction',
				'click .gp-result-coor-share' : 'clickShare',
				'click .gp-result-coor-lat' : 'clickLat',
				'click .gp-result-coor-lng' : 'clickLng',
				'click .gp-result-action-info' : 'clickInfo',
				'click .gp-result-action-coor' : 'clickCoor',
				'click .gp-result-action-fav' : 'clickFav'
			},
			style : function(css, win) {
				var self = this, textPadding = win.ptMin(0.035);
				// itemWidth = win.ptWidth(0.8);
				// console.log(win.width, textPadding, itemWidth,
				// this.$el.width());
				css({
					'li' : {
						'margin-bottom' : win.pxMin(0.008)
					},
					'.gp-result-item a' : {
						'padding' : textPadding + 'px'
					},
					'.gp-result-coor' : {
						'border-top' : win.pxMin(0.002) + ' solid #666'
					},
					'.gp-result-action a, .gp-result-coor a' : {
						'padding' : win.pxMin(0.03) + ' 0',
					},
					'.gp-result-item img, .gp-result-item > span' : {
						'width' : win.pxMin(0.08)
					},
					'.gp-result-action img, .gp-result-coor img' : {
						'width' : win.pxMin(0.05)
					}
				});
			},
			done : function() {
				// Enable css button click animation
				// this.$el.button('a:not(.gp-result-item-main)');
				this.$el.button('a', true);
				this.$el.button('.gp-result-item > span', true);
				// window.Notify.pop('Latitude clipboard very very long');
			},
			addResultItem : function(data) {
				// console.log(data);
				if (!_.has(data, 'formatted_address')) {
					if (_.has(data, 'vicinity')) {
						data.formatted_address = data.vicinity;
					} else {
						data.formatted_address = '';
					}
				}

				var item = $($.parseHTML(App.compile('item', data)));
				item.data('coordinate', data.geometry.location);
				item.data('address', data.formatted_address);
				item.data('name', data.name);
				item.data('id', data.id);
				this.$el.append(item);
			},
			copyToClipboard : function(str) {
				if (window.cordova && cordova.plugins && cordova.plugins.clipboard) {
					cordova.plugins.clipboard.copy(str);
				}
			},
			getUrl : function(type, coor) {
				switch (type) {
				case 'default':
					return 'geo:' + coor.lat + ',' + coor.lng;
				case 'sygic':
					return 'com.sygic.aura://coordinate|' + coor.lng + '|' + coor.lat + '|show';
				case 'sygic-old':
					return 'http://com.sygic.aura/coordinate|' + coor.lng + '|' + coor.lat + '|show';
				default:
					return false;
				}
			},
			clickAction : function(e) {
				$(e.currentTarget).closest('li').find('.gp-result-action').toggle().nextAll().hide();
			},
			clickItem : function(e, type) {
				var self = this, item = $(e.currentTarget).closest('li'), coor = item.data('coordinate'), name = item
						.data('name'), type = type || window.Config.app.placeNavigator;
				$.openIntent(type, coor);
				// console.log('Intent Geo', type, coor);
				// if (window.plugins && window.plugins.webintent) {
				// window.plugins.webintent.startActivity({
				// action: window.plugins.webintent.ACTION_VIEW,
				// url: this.getUrl(type, coor)
				// },
				// function() {},
				// function(err) {
				// if (type === 'sygic') {
				// if (window.Config.maps.sygic) {
				// self.clickItem(e, 'sygic-old');
				// } else {
				// window.Notify.pop('Error, Sygic is not installed');
				// }
				// } else {
				// window.Notify.pop('Error, No navigator installed');
				// }
				// });
				// }
				if (_.indexOf(window.Config.searchHistory, name) === -1) {
					window.Config.searchHistory.unshift(name);
				}
			},
			clickInfo : function(e) {
				var item = $(e.currentTarget).closest('li'), address = item.data('name') + ' ' + item.data('address');
				console.log('Intent Address', address);
				if (window.plugins && window.plugins.webintent) {
					window.plugins.webintent.startActivity({
						action : window.plugins.webintent.ACTION_VIEW,
						url : 'https://www.google.com/search?q=' + address
					}, function() {
					}, function() {
						window.Notify.pop('Failed to submit request')
					});
				}
			},
			clickCoor : function(e) {
				$(e.currentTarget).closest('li').find('.gp-result-coor').toggle();
			},
			clickShare : function(e) {
				var item = $(e.currentTarget).closest('li'), coor = item.data('coordinate'), name = item.data('name'), msg = name
						+ ' (Coordinate: ' + coor.lat + ' N, ' + coor.lng + ' E)';
				// window.Notify.pop('Latitude & longitude copied');
				console.log('Intent Share', msg);
				if (window.plugins && window.plugins.webintent) {
					var extras = {};
					extras[window.plugins.webintent.EXTRA_TEXT] = msg;
					window.plugins.webintent.startActivity({
						action : window.plugins.webintent.ACTION_SEND,
						type : 'text/plain',
						extras : extras
					}, function() {
					}, function() {
						window.Notify.pop('Failed to submit request')
					});
				}
			},
			clickLat : function(e) {
				var coor = $(e.currentTarget).closest('li').data('coordinate');
				this.copyToClipboard(coor.lat);
				window.Notify.pop('Latitude copied');
			},
			clickLng : function(e) {
				var coor = $(e.currentTarget).closest('li').data('coordinate');
				this.copyToClipboard(coor.lng);
				window.Notify.pop('Longitude copied');
			},
			clickFav : function(e) {
				var item = $(e.currentTarget).closest('li'), data = {
					id : item.data('id'),
					name : item.data('name'),
					address : item.data('address'),
					coordinate : item.data('coordinate')
				};
				window.Config.favorites[data.id] = data;
				App.fs.write(window.Config.favFile, JSON.stringify(window.Config.favorites), {
					clear : true
				});
				window.Notify.pop('Added to favorites');
				window.Body.search.favorites.updateList();
			}
		});
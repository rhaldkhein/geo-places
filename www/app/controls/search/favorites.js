App.View.Favorites = Backbone.View
		.extend({
			/**
			 * @memberOf App.View.Favorites
			 */
			id : 'gp-favorites',
			tagName : 'div',
			className : 'anchor top-left bottom-right',
			selected : null,
			events : {
				'touchstart .gp-action' : 'touchStartAction',
				'click .gp-favitem > a' : 'clickItem',
				'click .gp-action-rename' : 'clickRename',
				'click .gp-action-moreinfo' : 'clickInfo',
				'click .gp-action-share' : 'clickShare',
				'click .gp-action-delete' : 'clickDelete',
				'blur input' : 'blurInput'
			},
			style : function(css, vp) {
				css({
					'' : {
						'padding' : vp.pxMin(0.05),
						'padding-top' : vp.pxMin(0.03),
					},
					'h3' : {
						'border-bottom' : vp.pxMin(0.002) + ' solid #aaa',
						'padding' : vp.pxMin(0.02) + ' ' + vp.pxMin(0.04),
						'margin-bottom' : vp.pxMin(0.02)
					},
					'h3 img' : {
						'height' : vp.pxMin(0.06)
					},
					'ul.gp-favlist > li > a' : {
						'padding' : vp.pxMin(0.03) + ' ' + vp.pxMin(0.05),
						'border-bottom' : vp.pxMin(0.002) + ' solid #464646'
					},
					'ul.gp-favlist > li:first-child > a' : {
						'border-top' : vp.pxMin(0.002) + ' solid #464646'
					},
					'ul.gp-favlist' : {
						'margin' : vp.pxMin(0.05),
						'margin-top' : vp.pxMin(0.17),
						'margin-bottom' : vp.pxMin(0.25)
					},
					'ul.gp-action' : {},
					'ul.gp-action a' : {
						'padding' : vp.pxMin(0.03) + ' ' + vp.pxMin(0.05),
						'border-bottom' : vp.pxMin(0.002) + ' solid #7a7a7a'
					},
					'ul.gp-action li:first-child a' : {
						'border-top' : vp.pxMin(0.002) + ' solid #7a7a7a'
					},
					'input' : {
						'font-size' : vp.pxMin(0.04),
						'padding' : vp.pxMin(0.015) + ' ' + vp.pxMin(0.05)
					},
					'table .el-action' : {
						'width' : vp.pxMin(0.18),
						'border-left' : vp.pxMin(0.002) + ' solid #8a8a8a'
					}
				});
			},
			init : function() {
				this.cbFunc = _.bind(this.cbFunc, this);
				this.overlay = new App.View.Overlay();
				this.action = new App.View.FavAction({
					control : this
				});
			},
			done : function() {
				var self = this, list = this.$el.find('.gp-favlist'), active, item, button, actions;
				this.$el.button('a', true, function(el) {
					button = $(el);
					item = button.closest('li');
					actions = item.find('ul');
					self.selected = button;
					self.showAction();
					// actions.show();
					// $(document).off($.isMobile ? 'touchstart' : 'mousedown',
					// self.cbFunc);
					// $(document).on($.isMobile ? 'touchstart' : 'mousedown',
					// self.cbFunc);
					if (actions.position()
							&& self.$el.find('.gp-favlist').height() < actions.position().top
									+ actions.outerHeight(true)) {
						actions.css('margin-top', '-' + (actions.outerHeight(true) + button.outerHeight(true)) + 'px');
					}
					setTimeout(function() {
						button.removeClass('active');
					}, 300)
				});
				$(window).on('resize', function() {
					active = $(document.activeElement);
					if (active.prop('tagName').toLowerCase() === 'input') {
						list.scrollTop((active.position().top - (list.height() / 2)) + list.scrollTop());
					}
				});
				this.updateList();
			},
			cbFunc : function(e) {
				var container = this.$el.find('ul.gp-favlist ul:visible');
				if (!container.is(e.target) && container.has(e.target).length === 0) {
					container.hide();
					this.$el.find('.gp-favlist').css('pointer-events', 'auto');
					$(document).off($.isMobile ? 'touchstart' : 'mousedown', this.cbFunc);
				}
			},
			updateList : function() {

				if (_.isEmpty(window.Config.favorites)) {
					this.hide();
					setTimeout(function() {
						window.Body.search.logo.show();
					}, 0);
					return;
				} else {
					this.show();
					setTimeout(function() {
						window.Body.search.logo.hide();
					}, 0);
				}

				var item, list = this.$el.find('ul');
				list.empty();
				_.each(window.Config.favorites, function(val, key) {
					item = $($.parseHTML(App.compile('favitem', val)));
					item.data('data', val);
					list.append(item);
				});
				this.sortList();
			},
			showAction : function() {
				// this.action.show();
				this.overlay.show(this.action);
			},
			sortList : function(dec) {
				var $list = this.$el.find('ul.gp-favlist');
				$list.find('> li').sort(function(a, b) {
					if (dec === true)
						return ($(b).find('.el-text').text()) > ($(a).find('.el-text').text()) ? 1 : -1;
					else
						return ($(b).find('.el-text').text()) < ($(a).find('.el-text').text()) ? 1 : -1;
				}).appendTo($list);
			},
			clickItem : function(e) {
				var data = $(e.currentTarget).closest('li.gp-favitem').data('data');
				$.openIntent(window.Config.app.placeNavigator, data.coordinate);
				// if (window.plugins && window.plugins.webintent) {
				// window.plugins.webintent.startActivity({
				// action : window.plugins.webintent.ACTION_VIEW,
				// url : 'geo:' + data.coordinate.lat + ',' +
				// data.coordinate.lng
				// }, function() {
				// }, function() {
				// window.Notify.pop('Failed to submit request');
				// });
				// }
			},
			clickRename : function(e) {
				var $item = this.selected.closest('li.gp-favitem'), data = $item.data('data');
				$item.find('table').show();
				$item.find('input[type=text]').focus().val(data.name);
				this.cbFunc({
					target : this.el
				});
			},
			blurInput : function(e) {
				var $target = $(e.currentTarget), data = $target.closest('li.gp-favitem').data('data'), text = $(
						e.currentTarget).val();
				if (text.length) {
					window.Config.favorites[data.id].name = text;
					this.updateList();
					App.fs.write(window.Config.favFile, JSON.stringify(window.Config.favorites), {
						clear : true
					});
				}
				$(e.currentTarget).closest('table').hide();
			},
			clickInfo : function(e) {
				var data = this.selected.closest('li.gp-favitem').data('data'), address = data.name + ' '
						+ data.address;
				if (window.plugins && window.plugins.webintent) {
					window.plugins.webintent.startActivity({
						action : window.plugins.webintent.ACTION_VIEW,
						url : 'https://www.google.com/search?q=' + address
					}, function() {
					}, function() {
						window.Notify.pop('Failed to submit request');
					});
				}
				this.cbFunc({
					target : this.el
				});
			},
			clickShare : function(e) {
				var data = this.selected.closest('li.gp-favitem').data('data'), coor = data.coordinate, name = data.name, msg = name
						+ ' (Coordinate: ' + coor.lat + ' N, ' + coor.lng + ' E)';
				if (window.plugins && window.plugins.webintent) {
					var extras = {};
					extras[window.plugins.webintent.EXTRA_TEXT] = msg;
					window.plugins.webintent.startActivity({
						action : window.plugins.webintent.ACTION_SEND,
						type : 'text/plain',
						extras : extras
					}, function() {
					}, function() {
						window.Notify.pop('Failed to submit request');
					});
				}
				this.cbFunc({
					target : this.el
				});
			},
			clickDelete : function(e) {
				var data = this.selected.closest('li.gp-favitem').data('data');
				delete window.Config.favorites[data.id];
				this.updateList();
				App.fs.write(window.Config.favFile, JSON.stringify(window.Config.favorites), {
					clear : true
				});
				this.cbFunc({
					target : this.el
				});
			},
			touchStartAction : function(e) {
				console.log('Touchstart');
				e.preventDefault()
				return;
			}
		});
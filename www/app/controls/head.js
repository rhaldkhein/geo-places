App.View.Head = Backbone.View.extend({
	el : '#gp-head',
	events : {
		'click #gp-head-btn-main' : 'clickTest',
		'click #gp-head-btn-menu' : 'clickMenu'
	},
	clickTest : function() {
		Body.toggle('search');
	},
	clickMenu : function() {
		Body.toggle('menu');
	},
	style : function(css, win) {
		var self = this, headHeight = 0.13, headFontSize = 0.05, borderBottomSize = 0.006;
		// Topbar styles
		css({
			'' : {
				'height' : win.pxMin(headHeight),
				'background-color' : '#2790B0',
				'border-bottom' : win.pxMin(borderBottomSize) + ' solid #0D5C72',
				'z-index' : 200
			}
		});
		// Buttons styles
		css({
			'.ui-menu-bar button' : {
				'font-size' : win.pxMin(headFontSize),
				'font-weight' : 'bold',
				'height' : win.pxMin(headHeight),
				'background-color' : 'transparent',
				'color' : '#fff',
				'border' : 'none',
				'padding-left' : win.pxMin(0.035),
				'padding-right' : win.pxMin(0.035)
			},
			'.gp-head-logo' : {
				'width' : win.pxMin(0.09),
				'margin-right' : win.pxMin(0.01)
			},
			'.gp-head-icon' : {
				'width' : win.pxMin(0.06)
			},
			'.gp-head-status' : {
				'top' : self.$el.outerHeight() + 'px'
			}
		});
		// Apply css globally by providing true argument
		// Topbar height + topbar border
		css('#gp-body', 'top', win.pxMin(headHeight + borderBottomSize), true);
	},
	init : function() {
		// Show top bar header
		// this.show();
	},
	done : function() {
		// console.log(this.$el.context);
		this.show();
		this.$el.button('button');
		this.status = this.$el.find('.gp-head-status');

	},
	showStatus : function(msg, type) {
		this.status.children().text(msg);
		var color = '';
		switch (type) {
		case 'success':
			color = '79BD9A';
			break;
		case 'info':
			color = 'FF8048';
			break;
		case 'error':
			color = 'F50013';
			break;
		default:
			color = '81A8B8';
			break;
		}
		this.status.children().css('background-color', '#' + color).slideDown();
	},
	hideStatus : function(msg, type) {
		var self = this;
		if (msg != null) {
			this.showStatus(msg, type);
			setTimeout(function() {
				self.status.children().slideUp();
			}, 3000);
		} else {
			this.status.children().slideUp();
		}
	}
});
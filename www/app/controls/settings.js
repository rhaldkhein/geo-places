App.View.MainSettings = App.View.Settings.extend({
	id : 'gp-main-settings',
	controlBack : true,
	style : function(css, vp) {
		css({
			'.app-settings-label' : {
				'padding' : vp.pxMin(0.04)
			},
			'.app-settings-switch img' : {
				'width' : vp.pxMin(0.14)
			},
			'.app-settings-switch' : {
				'width' : vp.pxMin(0.23)
			},
			'.app-settings-container > div' : {
				'border-top' : vp.pxMin(0.002) + ' solid #666'
			},
			'.app-settings-container > div:first-child' : {
				'border-top' : 'none'
			},
			'.app-settings-item ul' : {
				'padding-top' : vp.pxMin(0.03),
				'margin-bottom' : vp.pxMin(0.04),
				'border-top' : vp.pxMin(0.002) + ' solid #aaa'
			},
			'.app-settings-item li > a, .app-settings-item li > div' : {
				'padding' : vp.pxMin(0.02) + ' ' + vp.pxMin(0.1)
			},
			'.app-settings-item li img' : {
				'width' : vp.pxMin(0.04),
				'margin-right' : vp.pxMin(0.02)
			},
			'.app-settings-item .app-settings-collapse img' : {
				'width' : vp.pxMin(0.07),
				'margin' : 0
			},
			'input' : {
				'padding' : vp.pxMin(0.02),
				'font-size' : vp.pxMin(0.045)
			},
			'.app-settings-multi img' : {
				'height' : vp.pxMin(0.04)
			},
			'.app-settings-input-button a' : {
				'width' : vp.pxMin(0.2),
				'padding' : vp.pxMin(0.025),
				'margin-left' : vp.pxMin(0.01)
			}
		});
	},
	init : function(opt) {
		// Setup the fields for parent settings
		window.opt = opt = {

			fields : _.extend(window.Config.app, {
				clearCache : function() {
					localStorage.clear();
					window.Notify.pop('Cache cleared!');
				},
				clearFavorites : function() {
					window.Config.favorites = {};
					window.Body.search.favorites.updateList();
					App.fs.write(window.Config.favFile, JSON.stringify(window.Config.favorites), {
						clear : true
					});
					window.Notify.pop('Favorites cleared!');
				}
			}),
			names : {
				placeNavigator : 'Navigator',
				coverage : 'Search Coverage'
			},
			descriptions : {
				cacheQuery : 'Enable cache on every search',
				clearCacheOnExit : 'Clears cache when the app closes',
				clearCache : 'Clear cache now',
				clearFavorites : 'Clear favorites now',
				placeNavigator : 'Select a navigator to start when you select a place',
				coverage : 'Select a search coverage'
			},
			controls : {
				placeNavigator : [ 'choicelist', 'string', 'default|Default', 'sygic|Sygic' ],
				coverage : [ 'choicelist', 'string', 'nearby|Nearby - (Needs your location)', 'global|Global' ]
			}
		};

		// Call parent init
		this.super.init.call(this, opt);

	},
	done : function() {
		this.super.done.call(this);
		// this.$el.button('.app-settings-label', true);
		this.$el.button('a', true);
	}
});
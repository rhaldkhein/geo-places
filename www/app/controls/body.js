App.View.Body = Backbone.View.extend({
	/**
	 * @memberOf App.View.Body
	 */
	el: '#gp-body',
	views: {},
	lastView: null,
	currentView: null,
	settings: null,
	toggleHistory: [],
	style: function(css, win) {
		var fontSize = 0.045;
		css({
			'': {
				'font-size': win.pxMin(fontSize)
			}
		});
	},
	done: function() {

		this.$el.empty();

		this.settings = new App.View.MainSettings();
		this.search = new App.View.Search();
		this.menu = new App.View.Menu();

		this.toggle('menu', this.menu);
		this.toggle('settings', this.settings);
		this.toggle('search', this.search, true);
		
		// this.search.show();

		this.settings.on('hide', function() {
			App.fs.write(window.Config.file, JSON.stringify(window.Config.app), {
				clear: true
			});
		});

	},
	toggle: function(name, view, show) {
		name = name.toLowerCase();
		if (view) {
			if (!(name in this.views)) {
				if (!show) {
					if (view.isVisible() === true)
						view.hide();
				} else {
					this.toggleHistory.push(name);
				}
				this.views[name] = view;
				this.append(view);
				var self = this;
				view.on('back', function() {
					self.toggleHistory.pop();
					self.toggle(_.last(self.toggleHistory));
				});
			}
		} else {
			if (name in this.views) {
				_.each(this.views, function(val) {
					if (val.isVisible() === true)
						val.hide();
				});
				this.views[name].show();
				var ndx = _.indexOf(this.toggleHistory, name),
					cnt = 0;
				if (ndx > -1)
					this.toggleHistory.splice(ndx, this.toggleHistory.length - ndx);
				if (_.last(this.toggleHistory) !== name)
					this.toggleHistory.push(name);
			}
		}
		this.lastView = this.lastView === null ? name : this.currentView;
		this.currentView = name;
	}
});

App.View.Menu = App.View.List.extend({
	id: 'gp-prime-menu',
	controlBack: true,
	style: function(css, win) {
		var textPadding = win.ptMin(0.035);
		css({
			'': {
				'padding-top': textPadding + 'px'
			},
			'.app-list-item': {
				'padding': textPadding + 'px',
				'margin-bottom': win.pxMin(0.01)
			}
		})
	}, 
	done: function() {
		// Call parent's done method
		this.super.done.call(this);
		var self = this;

		this.addItem('Settings');
		// this.addItem('Help');
		// this.addItem('About');

		this.on('itemclick', function(e) {
			Body.toggle($(e.item).text());
		});

		this.$el.button('li');
		this.append(new App.View.About());
		// this.$el.append($($.parseHTML(App.compile('about', {}))));
		// this.append();
	}
});
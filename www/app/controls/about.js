App.View.About = Backbone.View.extend({
	id: 'gp-about',
	style: function(css, vp) {
		css({
			'': {
				'padding': vp.pxMin(0.05)
			},
			'> *': {
				'padding': vp.pxMin(0.02) + ' ' + vp.pxMin(0.05),
			},
			'h3': {
				'margin-bottom': vp.pxMin(0.03),
				'border-bottom': vp.pxMin(0.002) + ' solid #a1a1a1'
			}
		});
	}
});
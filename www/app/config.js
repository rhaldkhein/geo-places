(function() {

	/*******************************************
	 * Environment Configuration
	 *******************************************/

	window.env = {
		project: {
			version: '1.1.0',
			name: 'Geo Places',
			code: 'com.kinnapps.geoplaces'
		},
		prod: false,
		dev: true
	};

	/*******************************************
	 * Application Configuration
	 *******************************************/

	window.Config = {
		project : env.project,
		// Application
		debug: true,
		application: {
			debug: false,
			autoRender: true,
			urlPrefix: ''
		},
		path: {
			storage: $.isMobile ? 'Android/data/' + window.env.project.code + '/' : '',
		},
		app: {
			cacheQuery: true,
			clearCacheOnExit: false,
			coverage: 'nearby',
			placeNavigator: 'default'
		},
		maps: {},
		favorites: {},
		searchHistory: [],
		// Orientation
		orientation: {
			lock: false,
			mode: "portrait"
		},
		location: null
	}

	// Set user config file path
	window.Config.file = window.Config.path.storage + 'config.json';
	window.Config.favFile = window.Config.path.storage + 'favorites.json';


})();
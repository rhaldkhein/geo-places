(function() {

	window._filemap = {
		tests : [

		],
		views : [ 
		         'app/view/search/search.html', 
		         'app/view/search/result.html',
		         'app/view/search/favorites.html', 
		         'app/view/search/item.html',
		         'app/view/favorites/favitem.html',
		         'app/view/favorites/favaction.html', 
		         'app/view/about.html'
		         ],
		libraries : [ 
		             'lib/jquery/jquery-filesystem.js',
		             'lib/lodash-2.4.1.min.js', 'lib/async-0.9.0.js',
		             'lib/backbone/backbone-1.1.2.js',
		             'lib/backbone/application/core.js',
		             'lib/backbone/application/list.js',
		             'lib/backbone/application/settings.js' 
		             ],
		application : [ 
		                'app/initialize.js', 
		                'app/controls/head.js',
		                'app/controls/body.js', 
		                'app/controls/notify.js',
		                'app/controls/menu.js', 
		                'app/controls/settings.js',
		                'app/controls/overlay.js', 
		                'app/controls/about.js',
		                'app/controls/search/search.js',
		                'app/controls/search/result.js',
		                'app/controls/search/favaction.js',
		                'app/controls/search/favorites.js', 
		                'app/instantiate.js' 
		                ]
	};

})();
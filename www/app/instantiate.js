(function() {

	App.on('starting', function(start) {

		console.log('----------------------------------------------------------');
		console.log('Starting...');
		var configFile = window.Config.file, favFile = window.Config.favFile;
		// Creating filesystem instance
		App.fs = new $.FileSystem();
		// Read / write config file and continue
		App.fs.on('init', function(next) {
			async.series([

			function(next) {
				if (window.appAvailability) {
					window.appAvailability.check('com.sygic.aura', function() {
						window.Config.maps.sygic = true;
						next();
					}, function() {
						window.Config.maps.sygic = false;
						next();
					});
				} else {
					next();
				}
			}, function(next) {
				App.fs.exist(favFile, function(exist) {
					if (exist) {
						console.info('Favorites file exist. Reading...');
						App.fs.read(favFile, function(err, data) {
							if (!err) {
								console.info('Favorites file loaded!');
								window.Config.favorites = JSON.parse(data);
								next();
							}
						});
					} else {
						console.info('Favorites file does not exist. Writing...');
						App.fs.write(favFile, JSON.stringify(window.Config.favorites), function(err) {
							if (!err)
								console.info('Favorites file written!');
						});
						next();
					}
				});
			}, function() {
				// Need to get user config
				App.fs.exist(configFile, function(exist) {
					if (exist) {
						console.info('Config file exist. Reading...');
						App.fs.read(configFile, function(err, data) {
							if (!err) {
								console.info('Config file loaded!');
								$.extend(window.Config.app, JSON.parse(data));
								// window.Config.app = JSON.parse(data);
								start();
							}
						});
					} else {
						console.info('Config file does not exist. Writing...');
						App.fs.write(configFile, JSON.stringify(window.Config.app), function(err) {
							if (!err)
								console.info('Config file written!');
						});
						start();
					}
				});
			} ]);
		});
		// Clear cache on exit
		document.addEventListener("pause", function() {
			if (window.Config.app.clearCacheOnExit) {
				localStorage.clear();
				console.log('Cache cleared!');
			}
		}, false);

	});

	App.on('started', function() {

		console.log('Started!');
		window.Notify = new App.View.Notify();
		window.Head = new App.View.Head();
		window.Body = new App.View.Body();
		
	});

})();
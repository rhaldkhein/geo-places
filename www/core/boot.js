(function($) {

	// Boot function
	function boot() {
		if (window.plugins && window.plugins.orientationLock) {
			if (Config.orientation.lock) {
				window.plugins.orientationLock.lock(Config.orientation.mode);
			}
		}
		$.getScript('core/index.js');
	}

	// Listen to window state
	window.onload = function() {

		// Load configuration before everything
		head.js.apply(window, [ 'app/config.js', 'app/filemap.js', function() {
			// Disable console for non debug
			if (window.env.prod) {
				console = {
					log : function() {
					},
					info : function() {
					}
				}
			}
			// Change loading size
			var h = $(window).height(), w = $(window).width();
			// $('.gp-loading').attr('width', (h > w ? w : h) * 0.08);
			// Boot up on device or cp
			if ($.isMobile) {
				// Listen to device state
				document.addEventListener('deviceready', function() {
					// Manual hide splash screen
					setTimeout(function() {
						if (navigator.splashscreen)
							navigator.splashscreen.hide();
					}, 700);
					boot();
				}, false);
			} else {
				// Listen to window state
				boot();
			}
		} ]);

		// $.getScript('app/config.js', function() {
		// $.getScript('app/filemap.js', function() {
		// // Change loading size
		// var h = $(window).height(), w = $(window).width();
		// // $('.gp-loading').attr('width', (h > w ? w : h) * 0.08);
		// // Boot up on device or cp
		// if ($.isMobile) {
		// // Listen to device state
		// document.addEventListener('deviceready', function() {
		// // Manual hide splash screen
		// setTimeout(function() {
		// if (navigator.splashscreen)
		// navigator.splashscreen.hide();
		// }, 700);
		// boot();
		// }, false);
		// } else {
		// // Listen to window state
		// boot();
		// }
		// });
		// });

	};

})(jQuery);
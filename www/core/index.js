(function() {

	var resource = {
		// Link view files
		views : _filemap.views,
		// Link test files
		tests : _filemap.tests,
		// Load library files
		libraries : [ _filemap.libraries, function() {
			// Loading application scripts
			head.js.apply(window, resource.application);
		} ],
		// Load application files
		application : [
				_filemap.application,
				function() {
					// Load templates
					var patt = /([^\/]+)(?=\.\w+$)/, xhrs = _.map(
							resource.views, function(val) {
								return $.get(val, function(res, stat) {
									$('<script/>').attr({
										type : 'text/template',
										id : val.match(patt)[0]
									}).html(res).appendTo('body');
								});
							});
					$.when.apply($, xhrs).then(function() {
						App.start();
					}, function() {
						console.log('Failed to Start!');
					});
					// Including test scripts
					_.each(resource.tests, function(val) {
						$.getScript(val);
					});
				} ]
	};
	
	// Start shaking your booty baby!
	head.js.apply(window, resource.libraries);

})();
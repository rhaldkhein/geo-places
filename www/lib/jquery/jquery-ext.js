(function($) {

	/**
	 * Method proxyer for jQuery methods
	 * 
	 * @param {string}
	 *            name Name of the method to proxy
	 * @return {function} Proxy function
	 */
	function makeProxy(name) {
		return function() {
			(this._JQ || (this._JQ = $(this)))[name].apply(this._JQ, arguments);
		};
	}

	/**
	 * Get the operating system of the machine
	 * 
	 * @return {string} Name if the operating system
	 */
	function getPlatform(retstr) {
		var name = "unknown", platforms = {
			windows : navigator.userAgent.indexOf('Win') != -1,
			mac : navigator.userAgent.indexOf('Mac') != -1,
			unix : navigator.userAgent.indexOf('X11') != -1,
			linux : navigator.userAgent.indexOf('Linux') != -1,
		};

		if (platforms.windows)
			name = 'windows';
		if (platforms.mac)
			name = 'mac';
		if (platforms.unix)
			name = 'unix';
		if (platforms.linux)
			name = 'linux';

		return retstr ? name : platforms;
	}

	/**
	 * Global helper function to queue a function onto next loop
	 * 
	 * @param {Function}
	 *            fn Function to call
	 * @return {void}
	 */
	window.nextTick = function(fn) {
		window.setTimeout(fn, 0);
	}

	// Extend jQuery
	$.extend($, {
		/**
		 * jQuery's custom event emitter
		 * 
		 * @type {Object}
		 */
		eventEmitter : {
			emit : makeProxy('trigger'),
			once : makeProxy('one'),
			on : makeProxy('on'),
			off : makeProxy('off'),
			has : function(name) {
				return $._data(this, 'events').hasOwnProperty(name);
			}
		},

		/**
		 * Detemine is the browser is mobile version or not
		 * 
		 * @type {Boolean}
		 */
		isMobile : ((navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/) || []).length > 0) ? true
				: false,

		/**
		 * Set the operating system of the machine
		 * 
		 * @type {string} Name if the operating system
		 */
		platform : getPlatform(),

		/**
		 * Set the separator for filesystem use
		 * 
		 * @type {string} The os specific separator
		 */
		separator : (function() {
			return (getPlatform().windows) ? '\\' : '/';
		})(),

		/**
		 * Flatten an object. Ex: {'obj.prop1.subprop2' : 'value' }
		 * 
		 * @param {object}
		 *            ob Object to flatten
		 * @return {object} Flattened object
		 */
		flatten : function(ob) {
			var toReturn = {};
			for ( var i in ob) {
				if (!ob.hasOwnProperty(i))
					continue;
				if ((typeof ob[i]) == 'object') {
					var flatObject = $.flatten(ob[i]);
					for ( var x in flatObject) {
						if (!flatObject.hasOwnProperty(x))
							continue;
						toReturn[i + '.' + x] = flatObject[x];
					}
				} else {
					toReturn[i] = ob[i];
				}
			}
			return toReturn;
		}

	});

	/**
	 * Simulate fake :active css pseudo class in non-link elements
	 */
	var className = 'active', delay = 500;
	$.fn.button = function(selector, scroll, callback) {
		selector = selector || '';
		var timeout = null, cleared = false, active = false, button = null, fnTouchEnd = function(e) {
			e = e || {};
			if (!active && !cleared && e.type !== 'touchmove') {
				// Tap
				if (button != null)
					button.addClass(className);
				active = true;
				setTimeout(fnTouchEnd, scroll ? (delay / 2) : 0);
			} else {
				if (button != null)
					button.removeClass(className);
				active = false;
			}
			clearTimeout(timeout);
			cleared = true;
		};
		// if ($.isMobile) {
		this.on(($.isMobile ? 'touchstart' : 'mousedown'), selector, function() {
			var self = this;
			button = $(this);
			// Reset clear and active flag
			cleared = false;
			active = false;
			// Delay adding class flag so that we can determine tap and scroll
			timeout = setTimeout(function() {
				button.addClass(className);
				active = true;
				if (callback)
					callback(self);
			}, scroll ? delay : 0);
		}).on(($.isMobile ? 'touchend' : 'mouseup'), selector, fnTouchEnd).on('touchcancel', selector, fnTouchEnd).on(
				'touchmove', selector, function(e) {
					if (!cleared) {
						fnTouchEnd(e);
					}
				});
		// } else {
		// this.on('mousedown', selector, function() {
		// button = $(this).addClass(className);
		// }).on('mouseup', selector, function() {
		// button.removeClass(className);
		// });
		// }
	};

})(jQuery);
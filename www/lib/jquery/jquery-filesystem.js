(function($) {

	/**
	 * We need the Blob constructor for blob data.
	 */
	window.Blob = window.BlobBuilder || window.Blob;

	/**
	 * Constructor for file system.
	 *
	 * @param {object} 		opt 		Options for constucting file system
	 */
	function FileSystem(opt) {
		// Options
		opt = opt || {};
		// Prepare native interface
		window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		window.webkitStorageInfo = window.webkitStorageInfo || navigator.webkitTemporaryStorage || navigator.webkitPersistentStorage;
		// Mobile flag
		this.isMobile = ((navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/) || []).length > 0) ? true : false;
		// Instance variables
		this.isInit = false;
		this.type = opt.type || window.PERSISTENT;
		this.size = (opt.size || (this.isMobile ? 0 : 1)) * 1024 * 1024;
		this.name = null;
		this.filesystem = null;
		this.lastWriteArgs = null;
		// Bind callbacks
		this._onInit = this._onInit.bind(this);
		this._onError = this._onError.bind(this);
		// Initialize
		this._init();
	}

	/**
	 * Add event emitter feature.
	 */
	$.extend(FileSystem.prototype, $.eventEmitter);

	/**
	 * Callback function to trigger on file system initialization error.
	 *
	 * @param  {Error}		fs 			The file system error
	 * @return {void}
	 */
	FileSystem.prototype._onError = function(err) {
		console.log('Error', err);
		var self = this;
		if (err.code === FileError.QUOTA_EXCEEDED_ERR) {
			if (window.webkitStorageInfo) {
				window.webkitStorageInfo.requestQuota(
					this.type,
					this.size,
					function(grantedBytes) {
						window.requestFileSystem(
							self.type,
							grantedBytes,
							self._onInit,
							self._onError
						);
					},
					this._onError
				);
			} else {
				throw new Error('Unable to initialize. Missing webkitStorageInfo method.');
			}
		}
		this.emit('error', err);
	};

	/**
	 * Callback function to trigger when file system is initialized.
	 *
	 * @param  {FileSystem}	fs 			The file system
	 * @return {void}
	 */
	FileSystem.prototype._onInit = function(fs) {
		this.name = fs.name;
		this.filesystem = fs.root;
		this.isInit = true;
		this.emit('init');
	};

	/**
	 * Initialize file system.
	 *
	 * @return {void}
	 */
	FileSystem.prototype._init = function() {
		if (window.requestFileSystem) {
			if (this.isMobile) {
				window.requestFileSystem(
					this.type,
					this.size,
					this._onInit,
					this._onError
				);
			} else {
				if (window.webkitStorageInfo) {
					var self = this;
					window.webkitStorageInfo.requestQuota(
						this.type,
						this.size,
						function(grantedBytes) {
							window.requestFileSystem(
								self.type,
								grantedBytes,
								self._onInit,
								self._onError
							);
						},
						this._onError
					);
				} else {
					throw new Error('Unable to initialize. Missing webkitStorageInfo method.');
				}
			}
		} else {
			throw new Error('Unable to initialize. Missing requestFileSystem method.');
		}
	};

	/**
	 * Actual writing process.
	 *
	 * @param  {string}   	filename 	Path to file
	 * @param  {object}   	data     	Any object type to write on file
	 * @param  {object}   	options  	Options parameters
	 * @param  {function} 	callback 	Function to trigger when completed
	 * @return {void}
	 */
	FileSystem.prototype._write = function(filename, data, options, callback) {
		// Return if not initialized
		if (!this.isInit) return;
		// Record write data
		this.lastWriteArgs = arguments;
		// Fix filename for specific platform
		filename = $.platform.windows ? filename.replace(/\//g, '\\') : filename.replace(/\\/g, '/');
		// Fix optional parameters
		if ($.type(options) === 'function') {
			callback = options;
			options = {};
		} else {
			callback = callback || function() {};
			options = options || {};
		}
		// Stick on me baby
		var self = this;
		// Create directory first
		self.directory(self.getDirectoryPath(filename), function(err, directory) {
			if (!err) {
				// Only get the name of the file
				filename = self.getFileName(filename);
				// Write the file baby
				directory.getFile(filename, {
						// Create file if it does not exist
						create: true
					}, function(fileEntry) {
						// Give me someone who can write
						fileEntry.createWriter(function(fileWriter) {
								var truncated = false,
									written = false,
									write = function() {
										// If opt-in to append, then fast forwards file pointer to end of file
										if (options.append) {
											fileWriter.seek(fileWriter.length);
										}
										// Write data as string
										if ($.type(data) === 'string') {
											// Add newline for newline option
											if (options.newline) {
												data = (options.clear ? '' : '\n') + data;
											}
											// Fix string for windows
											if ($.platform.windows) {
												data = new Blob([data], {
													type: 'text/plain'
												});
											}
										}
										// Write me baby
										written = true;
										fileWriter.write(data);
									};
								// Callback for success write or truncate
								fileWriter.onwrite = function(e) {
									if (options.clear && truncated && !written) {
										write();
									} else if (written) {
										this.lastWriteArgs = null;
										callback(false, e);
									} else {
										callback(e);
									}
								};
								// Callback for error
								fileWriter.onerror = function(e) {
									callback(e);
								};
								// Trigger trucate or write
								if (options.clear && !truncated) {
									truncated = true;
									fileWriter.truncate(0);
								} else {
									write();
								}
							},
							callback
						);
					},
					callback
				);
			}
		});

	};

	/**
	 * Navigate entry to directory. It creates if it does not exist?
	 *
	 * @param  {directory}  directory 	Path to directory
	 * @param  {callback} 	callback  	Function to trigger when completed
	 * @return {void}
	 */
	FileSystem.prototype.directory = function(directory, callback) {
		if (!this.isInit) return;
		// Statment for changing directory
		var root = this.filesystem,
			dirs = directory.split($.separator).reverse(),
			create = function(dir) {
				root.getDirectory(dir, {
					create: true
				}, success, callback);
			},
			success = function(entry) {
				root = entry;
				if (dirs.length > 0) {
					create(dirs.pop());
				} else {
					callback(false, entry);
				}
			};
		if (dirs.length > 0 && dirs[0] !== '') {
			create(dirs.pop());
		} else {
			callback(false, root);
		}
	};

	/**
	 * Get the name of a file.
	 *
	 * @param  {string} 	filename 	Path to file
	 * @return {boolean}
	 */
	FileSystem.prototype.getFileName = function(filename) {
		return filename.replace(/^.*[\\\/]/, '');
	};

	/**
	 * Get the parent directory of a file.
	 *
	 * @param  {string} 	filename 	Path to file
	 * @return {boolean}
	 */
	FileSystem.prototype.getDirectoryPath = function(filename) {
		return filename.substring(0, filename.lastIndexOf($.separator));
	};

	/**
	 * Checks a file or directory existance.
	 *
	 * @param  {string}   	filename 	File or directory to check
	 * @param  {function} 	callback 	Function to call when completed
	 * @return {void}
	 */
	FileSystem.prototype.exist = function(filename, callback) {
		if (!this.isInit) return;
		if (filename.search(/\.[^\/\\]+$/) > -1) {
			this.filesystem.getFile(filename, {
				create: false
			}, function() {
				callback(true);
			}, function(err) {
				callback(false, err);
			});
		} else {
			this.filesystem.getDirectory(filename, {
				create: false
			}, function() {
				callback(true);
			}, function(err) {
				callback(false, err);
			});
		}
	};

	/**
	 * Write data to a file.
	 *
	 * @param  {string}   	filename 	Name of the file
	 * @param  {object}   	data     	Any type of object to append
	 * @param  {boolean}   	clear  		Optional: Clear the file or not
	 * @param  {function} 	callback 	Optional: Function to trigger when completed - (error, data)
	 * @return {void}
	 */
	FileSystem.prototype.write = function(filename, data, clear, callback) {
		// Return if not initialized
		if (!this.isInit) return;
		// Fix optional parameters
		if ($.type(clear) === 'function') {
			callback = clear;
			clear = true;
		} else {
			callback = callback || function() {};
			clear = clear ? true : false;
		}
		this._write(filename, data, {
			clear: clear
		}, callback);
	};

	/**
	 * Rewrite last write attempt, this is might be usefull when write error occured.
	 *
	 * @return {void}
	 */
	FileSystem.prototype.rewrite = function() {
		this._write.apply(this, Array.prototype.slice.call(this.lastWriteArgs));
	}

	/**
	 * Appends data to a file.
	 *
	 * @param  {string}   	filename 	Name of the file
	 * @param  {object}   	data     	Any type of object to append
	 * @param  {boolean}   	newline  	Optional: Append with line break or not
	 * @param  {function} 	callback 	Optional: Function to trigger when completed - (error, data)
	 * @return {void}
	 */
	FileSystem.prototype.append = function(filename, data, newline, callback) {
		// Return if not initialized
		if (!this.isInit) return;
		// Fix optional parameters
		if ($.type(newline) === 'function') {
			callback = newline;
			newline = true;
		} else {
			callback = callback || function() {};
			newline = newline ? true : false;
		}
		this._write(filename, data, {
			append: true,
			newline: newline
		}, callback);
	};

	FileSystem.prototype.readBinary = function(filename, callback) {
		// Return if not initialized
		if (!this.isInit) return;
		// Read buffer now
		this.read(filename, {
			type: 'binary'
		}, callback);
	};

	FileSystem.prototype.readBuffer = function(filename, callback) {
		// Return if not initialized
		if (!this.isInit) return;
		// Read buffer now
		this.read(filename, {
			type: 'buffer'
		}, callback);
	};

	/**
	 * Read a file with optional options and callback.
	 *
	 * Ex: read('path/to/directory/file.txt');
	 *     read('path/to/directory/file.txt', {readAs: 'binary'});
	 *     read('path/to/directory/file.txt', {readAs: 'buffer'}, function() {});
	 *
	 * @param  {string}   	filename 	Full file name
	 * @param  {object}   	options  	Optional parameters - {type}
	 * @param  {function} 	callback 	Optional function to trigger when success or error - (error, data)
	 * @return {void}
	 */
	FileSystem.prototype.read = function(filename, options, callback) {
		// Return if not initialized
		if (!this.isInit) return;
		// Fix optional parameters
		if ($.type(options) === 'function') {
			callback = options;
			options = {};
		} else {
			callback = callback || function() {};
			options = options || {};
		}
		// Stick on me!
		var self = this;
		// Read the file
		this.filesystem.getFile(filename, {
				// Do NOT create file if it does not exist
				create: false
			}, function(fileEntry) {
				fileEntry.file(function(file) {
						// Give me someone who can read
						var reader = new FileReader();
						// Callback for success read
						reader.onload = function(e) {
							callback(false, this.result);
						};
						// Callback for error
						reader.onerror = function() {
							callback(true);
						}
						// Select which reading procedure will be used
						switch (options.type) {
							case 'url':
								reader.readAsDataURL(file);
								break;
							case 'binary':
								reader.readAsBinaryString(file);
								break;
							case 'buffer':
								reader.readAsArrayBuffer(file);
								break;
							default:
								reader.readAsText(file);
						}
					},
					// Let the filesystem trigger the callback
					callback
				);
			},
			// Let the filesystem trigger the callback
			callback
		);
	};

	/**
	 * Delete or remove a file.
	 *
	 * @param  {string}   	filename 	File or directory to delete
	 * @param  {function} 	callback 	Function to call when completed
	 * @return {void}
	 */
	FileSystem.prototype.remove = function(filename, callback) {
		this.filesystem.getFile(filename, {
				// Do NOT create file if it does not exist
				create: false
			}, function(fileEntry) {
				callback = callback || function() {};
				fileEntry.remove(function() {
					callback(false);
				}, callback);
			},
			// Let the filesystem trigger the callback
			callback
		);
	};


	// Attach FileSystem to jQuery object
	$.FileSystem = FileSystem;

})(jQuery);
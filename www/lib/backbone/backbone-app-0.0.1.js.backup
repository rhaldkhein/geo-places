(function($) {
	"use strict";

	if (_.isUndefined(window.Backbone))
		throw new Error('Backbone is required. Version <= v1.1.2');

	// Application
	var Application = function() {

		// Default options
		this.options = {
			autoRender: true,
			urlPrefix: '',
			urlSuffix: '',
			templateCompiler: null
		};

		// Object namespaces
		this.Model = {};
		this.Collection = {};
		this.View = {};
		this.Data = {};

		// Hash callback storage
		this._hash = [];

		// Cache storage
		this._templates = {};

		// Function that compile template
		this.compiler = null;

		// Set window properties
		var $window = $(window),
			height = $window.height(),
			width = $window.width();

		// Expose window properties
		this.window = {
			_px: 'px',
			height: height,
			width: width,
			max: height > width ? height : width,
			min: height < width ? height : width,
			pxMax: function(val) {
				return Math.round(this.max * val) + this._px;
			},
			pxMin: function(val) {
				return Math.round(this.min * val) + this._px;
			}
		};

		// Setup a default template compiler, which is underscore's
		if (this.options.templateCompiler === null) {
			this.options.templateCompiler = function(template, data) {
				var compile = _.template(template);
				return compile(data);
			};
		}

		// Binding all methods with this
		_.bindAll(this.window);

	};

	// Application methods
	_.extend(Application.prototype, Backbone.Events, {

		// Cofiguration and export the application
		configure: function(config) {
			_.extend(this.options, config || {});
			return this;
		},
		// Pre initialize function
		_pre_initialize: function() {
			var self = this;
			// Reference local storage
			this.Data = localStorage || {};
			// Models
			_.each(this.Model, function(model, key) {
				model.prototype.url = self.options.urlPrefix + model.prototype.url + self.options.urlSuffix;
			});
			// Collections
			_.each(this.Collection, function(collection, key) {
				collection.prototype.url = self.options.urlPrefix + collection.prototype.url + self.options.urlSuffix;
			});
			// Listen to hashchange
			history.fromApp = false;
			$(window).on('hashchange', function(e) {
				e.preventDefault();
				if (location.hash === '') {
					if (this._hash.length > 0) {
						if (!history.fromApp)
							(this._hash.pop())();
						if (this._hash.length > 0)
							location.hash = 'back';
					}
				}
				history.fromApp = false;
			}.bind(this));
		},

		// Boot up the application
		_initialize: function() {
			// Templates
			var self = this,
				element = null,
				fnName = null;
			// Get predegined templates
			$('script[type="text/template"]').each(function() {
				element = $(this);
				self._templates[element.attr('id')] = element.html().replace(/[\f\n\r\t\v]*/g, "");
				element.remove();
			});
			// Views
			_.each(this.View, function(view, key) {
				// Enable case insensitive
				fnName = key.toLowerCase();
				// Set the template
				view.prototype.template = fnName;
				// Set the name
				view.prototype.name = key;
			});
		},

		// Start the application, also starts the Backbone's history
		start: function() {
			var self = this,
				proceed = function(cont) {
					if (cont === false) {
						// Trigger start error
						self.trigger('error');
					} else {
						// Initialize application object
						self._initialize();
						// Set template compiler
						self.compiler = self.options.templateCompiler;
						// Trigger started callback
						self.trigger('started');
					}
				};
			if (this._events && this._events['starting']) {
				// Call pre init handler
				self._pre_initialize();
				// Trigger starting callback for custom initialization
				this.trigger('starting', proceed);
			} else {
				proceed(true);
			}
		},

		// Add template to cache
		addTemplate: function(id, template, override) {
			if (this._templates[id] === undefined) {
				this._templates[id] = template.replace(/[\f\n\r\t\v]*/g, "");
			} else {
				if (override)
					this._templates[id] = template.replace(/[\f\n\r\t\v]*/g, "");
			}
		},

		// Get a template by element id
		getTemplate: function(id) {
			if (id) {
				if (this._templates[id] === undefined) {
					var element = $('script[type="text/template"]#' + id);
					var template = element.html();
					element.remove();
					this._templates[id] = template;
				}
				return this._templates[id];
			}
		},

		// Compile the template
		compile: function(name, data) {
			return this.compiler(this.getTemplate(name), data);
		},

		// Check if object is a model or a collection
		isModel: function(obj) {
			if (obj instanceof Backbone.Model)
				return 1;
			else if (obj instanceof Backbone.Collection)
				return 2;
			return 0;
		},

		// Get or set data storage
		data: function(key, val) {
			if (val) {
				this.Data[key] = val;
				return this;
			} else if (key) {
				return this.Data[key];
			} else {
				return this.Data;
			}
		},

	});

	// Instantiate application
	window.App = new Application();

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

	Backbone.Model.prototype.initialize = Backbone.Collection.prototype.initialize = function() {
		this.view = null;
		if (this.init)
			this.init.apply(this, arguments);
	};

	// Backbone.Collection Extension
	// -----------------------------

	// Extend Backbone's Collection prototype
	_.extend(Backbone.Collection.prototype, {
		// Return a new array of models with provided array of ids
		getArray: function(ids, field) {
			field = field || 'id';
			return this.filter(function(model) {
				return ids.indexOf(model.attributes[field]) !== -1;
			});
		},
		// Return a set of models, and fetch if not exist in colletion
		load: function(ids, data, callback, field) {
			var self = this,
				set, arr, result;
			// Fix optional paramaters
			if (_.isFunction(data)) {
				if (callback)
					field = callback;
				callback = data;
				data = {};
			}
			// If ids is string then wrap it into array
			if (!_.isArray(ids)) {
				// ids = [parseInt(ids, 10)];
				ids = (field === undefined) ? [parseInt(ids, 10)] : [ids];
				arr = false;
			} else if (ids.length === 0) {
				setTimeout(function() {
					callback(ids);
				}, 0);
				return;
			} else {
				arr = true;
			}
			// Fix frield param and defaults to id
			field = field || 'id';
			// Reduce the set of ids by remove the ids that has been fetched already
			set = _.difference(ids, this.pluck(field));
			if (set.length <= 0) {
				setTimeout(function() {
					result = self.getArray(ids, field);
					callback(arr ? result : result[0]);
				}, 0);
			} else {
				this.fetch({
					remove: false,
					data: _.extend(data, {
						type: 'set',
						field: field,
						set: set.join()
					}),
					success: function() {
						if (callback) {
							result = self.getArray(ids, field);
							callback(arr ? result : result[0]);
						}
					}
				});
			}
		},
		loadEach: function(ids, data, callback, field) {
			// Fix optional paramaters
			if (_.isFunction(data)) {
				if (callback)
					field = callback;
				callback = data;
				data = {};
			}
			this.load(ids, data, function(collection) {
				// Execute each single record
				_.each(collection, callback);
			}, field);
		}
	});

	// Backbone.View Extension
	// ------------------------

	Backbone.View.options = ['autoRender', 'persist', 'template', 'route', 'data', 'models'];

	// Extend Backbone's View prototype
	_.extend(Backbone.View.prototype, {
		// Data for template
		data: {},
		// The route responsibly for a view
		route: null,
		// Mark to display error template
		_error: false,
		// Data for error template
		_errorData: null,
		// Template for error
		_errorTemplate: null,
		// Set a variable for template
		set: function(name, value, listen) {
			listen = listen || false;
			if (_.isObject(name)) {
				if (_.isBoolean(value))
					listen = value;
				_.each(name, function(val, id) {
					if (App.isModel(val) !== 0) {
						this.models[id] = val;
						if (listen)
							this.listen(val);
					} else {
						this.data[id] = val;
					}
				}, this);
			} else {
				if (App.isModel(value) !== 0) {
					this.models[name] = value;
					if (listen)
						this.listen(value);
				} else {
					this.data[name] = value;
				}
			}
		},
		// Override view's initialize for us to auto trigger the render
		initialize: function() {
			// Configure this view
			this._config();
			// Generate selector
			this.selector = this.tagName + (this.el.id ? ('#' + this.el.id) : '') + (this.className ? ('.' + this.className) : '') + ' ';
			// Prepare a handler for back button
			if (this.controlBack) {
				this._back = this._back.bind(this);
			}
			// List of models and collections
			this.models = {};
			// Set to true when a view is rendered
			this.isRendered = false;
			// Set to true when a view is initialized
			this.isDone = false;
			// Trigger external init function
			if (this.init)
				this.init.call(this);
			// Trigger filters
			this._runFilters();
		},
		// Run all filters (entry point for cached views)
		_runFilters: function() {
			if (this.filters && !this._error) {
				_.each(this.filters, function(fn) {
					if (!this._error)
						App.Filter[fn].call(this);
				}, this);
			}
			if (this.filter && !this._error) {
				this.filter.call(this);
			}
			if (this.autoRender !== false) {
				if (App.options.autoRender) {
					this.data.LOADED = false;
					this.render();
					this.isDone = true;
					if (this.done) {
						this.done.call(this);
					}
				}
			}
		},
		// Renders a View that require some properties from context
		render: function(tpl, data) {
			if (App.options.debug)
				console.info('Rendering View:', this);
			// Predefine data loaded
			this.data.LOADED = this.data.LOADED || false;
			// Predefine data for template
			if (this.route) {
				this.data.ROUTE = this.route.route;
				this.data.QUERY = this.route.query;
				this.data.QUERYSTRING = _.isEmpty(this.data.QUERY) ? '' : $.param(this.data.QUERY);
			}
			// Get the template for 
			tpl = tpl || App.getTemplate(this.template);
			var modelData = {};
			// Extract data for template
			_.each(this.models, function(value, key) {
				if (value instanceof Backbone.Model || value instanceof Backbone.Collection) {
					modelData[key] = value.toJSON();
				}
			});
			// If persist, do not recompile, just re-show the element
			if (this.persist && this.isRendered) {
				this.$el.show();
			} else {
				// Compile template
				this.$el.html(App.compiler(tpl || this.$el.html(), _.extend({}, this.data, modelData, data)));
			}
			// Paste into the outlet element
			if (this.outlet) {
				$(this.outlet).html(this._error ? App.compiler(App.getTemplate(this._errorTemplate), this._errorData) : this.el);
			}
			// Re-delegate events
			this.delegateEvents();
			// Flag as rendered
			this.isRendered = true;
			// Trigger rendered callback
			if (this.rendered)
				this.rendered.call(this);
			// Return this object for chaining
			return this;
		},
		append: function(view, element) {
			if (view.isRendered) {
				var el = element ? this.$el.find(element) : this.$el;
				el.append(view.el);
			}
		},
		prepend: function(view, element) {
			if (view.isRendered) {
				var el = element ? this.$el.find(element) : this.$el;
				el.prepend(view.el);
			}
		},
		placeIn: function(view, element) {
			if (view.isRendered) {
				this.$el.find(element).html(view.el);
			}
		},
		listen: function(model) {
			if (model instanceof Backbone.Model) {
				model.view = this;
				if (App.options.autoRender) {
					model.on('change', function(e) {
						if (App.options.debug)
							console.info('Model Changed:', e);
						this.data.LOADED = true;
						if (this.autoRender !== false)
							this.render();
					}, this);
				}
			} else if (model instanceof Backbone.Collection) {
				model.view = this;
				if (App.options.autoRender) {
					var fn = function(e) {
						if (App.options.debug)
							console.info('Collection Changed:', e);
						this.data.LOADED = true;
						if (this.autoRender !== false)
							this.render();
					};
					model.on('sync', fn, this).on('add', fn, this).on('remove', fn, this);
				}
			}
		},
		show: function() {
			this.visible(true);
		},
		hide: function() {
			this.visible(false);
		},
		visible: function(bool, silent) {
			if (bool === false) {
				this.$el.hide();
				// Control with back button, remove the handler
				if (silent) {} else {
					if (this.controlBack) {
						var self = this;
						if (App._hash.length > 0 && App._hash[App._hash.length - 1] === this._back) {
							history.fromApp = true;
							history.back();
						}
						_.remove(App._hash, function(fn) {
							return fn === self._back;
						});
					}
				}
			} else {
				this.$el.css('display', '');
				this.delegateEvents();
				// Parse style if specified
				if (this.style) {
					this.$el.css('visibility', 'hidden');
					this.style(this._addStyle.bind(this), App.window);
					this.style = false;
					setTimeout(function() {
						this.$el.css('visibility', 'visible');
					}.bind(this), 0);
				}
				// Control with back button, add the handler
				if (this.controlBack) {
					App._hash.push(this._back);
					location.hash = 'back';
				}
			}
			return this;
		},
		isVisible: function() {
			return this.$el.is(':visible') || false;
		},
		error: function(data, name) {
			this._error = true;
			this._errorData = data || {};
			this._errorTemplate = name || 'error';
			this.render();
			// Revert back the mark
			this._error = false;
		},
		_addStyle: function(element, style, value, inside) {
			var $el = $('style#' + this.template),
				res = '',
				obj = null,
				iA = null,
				iB = null;
			if (!_.isPlainObject(element)) {
				obj = {};
				if (_.isPlainObject(style)) {
					obj[element] = style;
				} else {
					obj[element] = {};
					obj[element][style] = value;
				}
			} else {
				obj = element;
			}
			inside = _.isUndefined(inside);
			for (iA in obj) {
				res += (inside ? this.selector : '') + iA + '{';
				for (iB in obj[iA]) {
					res += iB + ':' + obj[iA][iB] + ';';
				}
				res += '} ';
			}
			if ($el.length > 0) {
				// Exisitng style is present, replace it
				$el.append(res);
			} else {
				// Create style element and append it to document
				$('<style/>').attr({
					id: this.template,
					type: 'text/css',
					rel: 'stylesheet'
				}).text(res).appendTo($('head'));
			}
		},
		_back: function() {
			this.visible(false, true);
		},
		_config: function() {
			this.options = this.options || {};
			_.extend(this, _.pick(this.options, this.constructor.options));
			_.extend(this.data, {
				LOADED: false,
				ROUTE: undefined,
				QUERY: undefined,
				QUERYSTRING: undefined
			});
		}
	});

})(jQuery);
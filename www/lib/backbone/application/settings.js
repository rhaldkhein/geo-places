/**
 * Settings control for application framework.
 */

/**
 * Inject the template for settings control, this can be overridden
 * by script tag in html / document file or child's template.
 *
 * Template requires app-list-container class
 */
App.addTemplate('settings', '');

/**
 * Settings control class
 */
App.View.Settings = Backbone.View.extend({
	_types: ['boolean', 'string', 'number', 'date', 'function', 'array'],
	_functions: {},
	className: 'app-settings',
	compile: false,
	events: {
		'click .app-settings-switch': 'clickCheckBox',
		'click .app-settings-label': 'clickItem',
		'click .app-settings-collapse': 'clickCollapse',
		'click .app-settings-control a': 'clickControl',
		'click .app-settings-control .el-close': 'clickArrayRemove',
		'submit form': 'submitForm'
	},
	images: {
		close: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgd2lkdGg9IjIzLjMzMnB4IiBoZWlnaHQ9IjIzLjMzM3B4IiB2aWV3Qm94PSIwIDAgMjMuMzMyIDIzLjMzMyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMjMuMzMyIDIzLjMzMyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8cGF0aCBmaWxsPSIjQ0NDQ0NDIiBkPSJNMTYuMDQzLDExLjY2N0wyMi42MDksNS4xYzAuOTYzLTAuOTYzLDAuOTYzLTIuNTM5LDAtMy41MDJsLTAuODc1LTAuODc1Yy0wLjk2My0wLjk2NC0yLjUzOS0wLjk2NC0zLjUwMiwwDQoJTDExLjY2Niw3LjI5TDUuMDk5LDAuNzIzYy0wLjk2Mi0wLjk2My0yLjUzOC0wLjk2My0zLjUwMSwwTDAuNzIyLDEuNTk4Yy0wLjk2MiwwLjk2My0wLjk2MiwyLjUzOSwwLDMuNTAybDYuNTY2LDYuNTY2bC02LjU2Niw2LjU2Nw0KCWMtMC45NjIsMC45NjMtMC45NjIsMi41MzksMCwzLjUwMWwwLjg3NiwwLjg3NWMwLjk2MywwLjk2MywyLjUzOSwwLjk2MywzLjUwMSwwbDYuNTY3LTYuNTY1bDYuNTY2LDYuNTY1DQoJYzAuOTYzLDAuOTYzLDIuNTM5LDAuOTYzLDMuNTAyLDBsMC44NzUtMC44NzVjMC45NjMtMC45NjMsMC45NjMtMi41MzksMC0zLjUwMUwxNi4wNDMsMTEuNjY3eiIvPg0KPC9zdmc+DQo=',
		arrowUp: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjE2cHgiIHZpZXdCb3g9IjAgMCAzMiAxNiIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMTYiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPHBvbHlnb24gZmlsbD0iI0NDQ0NDQyIgcG9pbnRzPSIwLjkyLDE0LjY2NSAzMS4wOCwxNC42NjUgMTYuMDAxLDEuMzM1IAkiLz4NCjwvZz4NCjwvc3ZnPg0K',
		switchOn: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjE2cHgiIHZpZXdCb3g9IjAgMCAzMiAxNiIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMTYiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPHBhdGggZmlsbD0iIzMzMzMzMyIgZD0iTTMyLDMuNzU4YzAtMS4xLTAuOS0yLTItMkgyYy0xLjEsMC0yLDAuOS0yLDJWMTRjMCwxLjEsMC45LDIsMiwyaDI4YzEuMSwwLDItMC45LDItMlYzLjc1OHoiLz4NCjwvZz4NCjxnPg0KCTxwYXRoIGZpbGw9IiMzOUI1NEEiIGQ9Ik0yNy4xMTMsMTAuODI4YzAsMC41NS0wLjQ1LDEtMSwxSDUuODg3Yy0wLjU1LDAtMS0wLjQ1LTEtMVY1LjE3MmMwLTAuNTUsMC40NS0xLDEtMWgyMC4yMjcNCgkJYzAuNTUsMCwxLDAuNDUsMSwxVjEwLjgyOHoiLz4NCjwvZz4NCjxnPg0KCTxwYXRoIGZpbGw9IiNCM0IzQjMiIGQ9Ik0zMiw1LjIzNWMwLDEuMS0wLjksMi0yLDJIMmMtMS4xLDAtMi0wLjktMi0yVjJjMC0xLjEsMC45LTIsMi0yaDI4YzEuMSwwLDIsMC45LDIsMlY1LjIzNXoiLz4NCjwvZz4NCjxnPg0KCTxwYXRoIGZpbGw9Im5vbmUiIGQ9Ik0zMiwyYzAtMS4xLTAuOS0yLTItMkgyQzAuOSwwLDAsMC45LDAsMnYxMmMwLDEuMSwwLjksMiwyLDJoMjhjMS4xLDAsMi0wLjksMi0yVjJ6Ii8+DQo8L2c+DQo8L3N2Zz4NCg==',
		switchOff: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjE2cHgiIHZpZXdCb3g9IjAgMCAzMiAxNiIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMzIgMTYiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPHBhdGggZmlsbD0iIzMzMzMzMyIgZD0iTTMyLDEyLjI0MmMwLDEuMS0wLjksMi0yLDJIMmMtMS4xLDAtMi0wLjktMi0yVjJjMC0xLjEsMC45LTIsMi0yaDI4YzEuMSwwLDIsMC45LDIsMlYxMi4yNDJ6Ii8+DQo8L2c+DQo8Zz4NCgk8cGF0aCBmaWxsPSIjMUExQTFBIiBkPSJNMjcuMTEzLDUuMTcyYzAtMC41NS0wLjQ1LTEtMS0xSDUuODg3Yy0wLjU1LDAtMSwwLjQ1LTEsMXY1LjY1NWMwLDAuNTUsMC40NSwxLDEsMWgyMC4yMjcNCgkJYzAuNTUsMCwxLTAuNDUsMS0xVjUuMTcyeiIvPg0KPC9nPg0KPGc+DQoJPHBhdGggZmlsbD0iI0IzQjNCMyIgZD0iTTMyLDEwLjc2NWMwLTEuMS0wLjktMi0yLTJIMmMtMS4xLDAtMiwwLjktMiwyVjE0YzAsMS4xLDAuOSwyLDIsMmgyOGMxLjEsMCwyLTAuOSwyLTJWMTAuNzY1eiIvPg0KPC9nPg0KPGc+DQoJPHBhdGggZmlsbD0ibm9uZSIgZD0iTTMyLDE0YzAsMS4xLTAuOSwyLTIsMkgyYy0xLjEsMC0yLTAuOS0yLTJWMmMwLTEuMSwwLjktMiwyLTJoMjhjMS4xLDAsMiwwLjksMiwyVjE0eiIvPg0KPC9nPg0KPC9zdmc+DQo=',
		checkOn: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTYgMTYiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBhdGggZmlsbD0iIzgwODA4MCIgZD0iTTAsMHYxNmgxNlYwSDB6Ii8+DQo8cmVjdCB4PSIxLjA5IiB5PSIxLjA5IiBmaWxsPSIjMzMzMzMzIiB3aWR0aD0iMTMuODIiIGhlaWdodD0iMTMuODIiLz4NCjxwb2x5Z29uIGZpbGw9IiMyOUFCRTIiIHBvaW50cz0iMTEuNzEsMy4yMDMgNi4xMjUsOC45NTcgNC4yNjksNy45MjIgMi41MzYsOS4xMzkgNi4xOTcsMTMuMjcgMTMuNDk0LDQuMzU5ICIvPg0KPC9zdmc+DQo=',
		checkOff: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNS4xLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHdpZHRoPSIxNnB4IiBoZWlnaHQ9IjE2cHgiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgMTYgMTYiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPHBhdGggZmlsbD0iIzgwODA4MCIgZD0iTTAsMHYxNmgxNlYwSDB6Ii8+DQo8cmVjdCB4PSIxLjA5IiB5PSIxLjA5IiBmaWxsPSIjMzMzMzMzIiB3aWR0aD0iMTMuODIiIGhlaWdodD0iMTMuODIiLz4NCjwvc3ZnPg0K'
	},
	init: function(opt) {

		opt = opt || {};
		this.options = opt;
		this.fields = opt.fields || {};
		this.fieldNames = opt.names || {};
		this.fieldDescription = opt.descriptions || {};
		this.fieldControl = opt.controls || {};

		// TODO! Add features
		this.fieldGroup = opt.fieldGroup || {};
		this.fieldDep = opt.fieldDep || [];
		this.groupOrder = opt.groupOrder || [];

		this.itemTemplates = {};
		this.lastHeight = $(window).outerHeight(true);

	},
	done: function() {
		var self = this;
		this.$container = this.$el.find('.app-settings-container');
		this._getItemTemplates();
		_.each(this.fields, function(val, key) {
			self._addItem($.type(val), key, val);
		});
		$(window).on('resize', function() {
			var active = $(document.activeElement),
				height = $(window).outerHeight(true);
			if (active.prop('tagName').toLowerCase() === 'input') {
				if (active.position().top + active.outerHeight(true) > self.$el.height()) {
					self.$el.scrollTop(self.$el.scrollTop() + (self.lastHeight - height));
				}
			}
			self.lastHeight = height;
		});
	},
	_addItem: function(type, name, val) {

		var fn = null,
			data = this._getTemplateData(type, name, val),
			$elem = $(App.compiler(this.itemTemplates[type], data));

		$elem.addClass('app-settings-item');
		$elem.data('name', name);
		$elem.data('display_name', data.name);
		$elem.data('type', type);

		fn = this['_' + type];
		if (_.isFunction(fn)) {
			fn.call(this, $elem, val);
		}

		this.$container.append(this._parseControl($elem));
	},
	_updateItem: function(elem, show) {

		var fn = null,
			name = elem.data('name'),
			type = elem.data('type'),
			value = this.fields[name],
			data = this._getTemplateData(type, name, value),
			$elem = $(App.compiler(this.itemTemplates[type], data));

		$elem.addClass('app-settings-item');
		$elem.data('name', name);
		$elem.data('display_name', data.name);
		$elem.data('type', type);

		fn = this['_' + type];
		if (_.isFunction(fn)) {
			fn.call(this, $elem, value);
		}

		elem.replaceWith(this._parseControl($elem));

		if (show)
			$elem.find('.app-settings-control').show();
	},
	_parseControl: function(elem) {
		var self = this;
		if (elem.data('name') in this.fieldControl) {
			var name = elem.data('name'),
				params = this.fieldControl[name].slice(),
				control = params.shift(),
				type = params.shift();

			if (control === 'checklist' || control === 'choicelist') {
				var tpl = '<ul style="display: none;" class="app-settings-control app-settings-' + control + '" data-type="' + type + '">';
				_.each(params, function(val) {
					val = val.split('|');
					if (val.length === 1) {
						val.push(val[0]);
					}
					tpl += '<li><a class="el-item" data-val="' + val[0] + '">';
					if (control === 'checklist') {
						var check = _.indexOf(self.fields[name], val[0]) !== -1;
						tpl += ('<img src="' + (check ? self.images.checkOn : self.images.checkOff) + '" data-check="' + (check ? 1 : 0) + '">');
					}
					tpl += '<span>' + val[1] + '</span></a></li>';
				});
				if (control === 'checklist') {
					tpl += '<li class="app-settings-collapse"><a><img src="' + self.images.arrowUp + '"></a></li>';
				}
				tpl += '</ul>';
				elem.data('control', control);
				elem.append(tpl);
			}

		}
		return elem;
	},
	_boolean: function(elem, val) {
		elem.data('value', val);
		elem.find('img').attr('src', val ? this.images.switchOn : this.images.switchOff);
	},
	_string: function(elem, val) {
		elem.data('value', val);
		if (!(elem.data('name') in this.fieldControl)) {
			var tpl = '<form><ul style="display: none;" class="app-settings-control">';
			tpl += '<li><div><input type="text" placeholder="New ' + elem.data('display_name') + '"></div></li>';
			tpl += '<li class="app-settings-input-button"><div><a class="el-ok">Ok</a><a class="el-cancel">Cancel</a></div></li>';
			tpl += '</ul></form>';
			elem.append(tpl);
		}
	},
	_array: function(elem, val) {
		elem.data('value', val);
		if (!(elem.data('name') in this.fieldControl)) {
			var self = this,
				tpl = '<form><ul style="display: none;" class="app-settings-control app-settings-multi">';
			_.each(val, function(item) {
				tpl += '<li><div>' + item + '<img class="el-close" src="' + self.images.close + '"></div></li>';
			});
			tpl += '<li><div><input type="text" placeholder="New ' + elem.data('display_name') + '"></div></li>';
			tpl += '<li class="app-settings-input-button"><div><a class="el-add">Add</a><a class="el-done">Done</a></div></li>';
			tpl += '</ul></form>';
			elem.append(tpl);
		}
	},
	_number: function(elem, val) {
		elem.data('value', val);
		if (!(elem.data('name') in this.fieldControl)) {
			var tpl = '<form><ul style="display: none;" class="app-settings-control">';
			tpl += '<li><div><input type="number" placeholder="New ' + elem.data('display_name') + '"></div></li>';
			tpl += '<li class="app-settings-input-button"><div><a class="el-ok">Ok</a><a class="el-cancel">Cancel</a></div></li>';
			tpl += '</ul></form>';
			elem.append(tpl);
		}
	},
	_function: function(elem, val) {
		this._functions[elem.data('name')] = val;
	},
	_getTemplateData: function(type, field, val) {
		var data = {
				name: null,
				description: null,
				value: null
			},
			name = this.fieldNames[field] ? this.fieldNames[field] : field.replace(/([A-Z])/g, ' $1').replace(/^./, function(str) {
				return str.toUpperCase();
			});
		data.name = name;
		data.description = this.fieldDescription[field] ? this.fieldDescription[field] : '';
		data.value = '';
		if (type === 'string' || type === 'number')
			data.value = val;
		return data;
	},
	_getItemTemplates: function() {
		var self = this,
			elem = null;
		_.each(this._types, function(val) {
			elem = self.$el.find('.app-settings-' + val);
			if (elem.length === 1) {
				self.itemTemplates[val] = _.unescape(elem.prop('outerHTML'));
				elem.remove();
			}
		});
	},
	clickItem: function(e) {
		var $target = $(e.currentTarget),
			$item = $target.closest('.app-settings-item'),
			$control = $item.find('.app-settings-control');

		if ($control.length !== 0) {
			$control.toggle();
		}
		switch ($item.data('type')) {
			case 'function':
				this._functions[$item.data('name')]();
				break;
			default:
		}
	},
	clickCheckBox: function(e) {
		var $target = $(e.currentTarget),
			$item = $target.closest('.app-settings-item'),
			name = $item.data('name');
		$item.data('value', !$item.data('value'));
		if ($item.data('value')) {
			$target.find('img').attr('src', this.images.switchOn);
			this.fields[name] = true;
		} else {
			$target.find('img').attr('src', this.images.switchOff);
			this.fields[name] = false;
		}
	},
	clickCollapse: function(e) {
		var $target = $(e.currentTarget);
		$target.closest('.app-settings-control').hide();
	},
	clickControl: function(e) {
		var $target = $(e.currentTarget),
			$item = $target.closest('.app-settings-item'),
			name = $item.data('name'),
			show = false,
			update = true,
			val = null;
		if ($target.hasClass('el-ok')) {
			val = $item.find('input').val();
			if ($item.data('type') === 'string') {
				val = $.trim(val.toString());
				if (val.length !== 0)
					this.fields[$item.data('name')] = val;
				else
					update = false;
			} else if ($item.data('type') === 'number') {
				val = parseInt(val);
				if (!_.isNaN(val))
					this.fields[$item.data('name')] = val;
			}
		} else if ($target.hasClass('el-add')) {
			val = $.trim($item.find('input').val());
			if (val.length !== 0) {
				this.fields[$item.data('name')].push(val);
				show = true;
			} else {
				update = false;
			}
		} else if ($target.hasClass('el-cancel') || $target.hasClass('el-done')) {
			$item.find('.app-settings-control').hide();
		} else if ($target.hasClass('el-item')) {
			var control = $item.data('control'),
				type = $item.find('.app-settings-control').data('type');
			if (control === 'checklist') {
				var checked = $target.find('img').data('check') ? 0 : 1,
					value = $target.data('val'),
					arr = this.fields[name];
				if (checked) {
					if (_.indexOf(arr, value) === -1) {
						if (type === 'number') {
							value = parseInt(value);
						} else if (type === 'string') {
							value = value.toString();
						}
						arr.push(value);
					}
				} else {
					arr.splice(_.indexOf(arr, value), 1);
				}
				show = true;
			} else if (control === 'choicelist') {
				if (type === 'number') {
					this.fields[name] = parseInt($target.data('val'));
				} else if (type === 'string') {
					this.fields[name] = $target.data('val').toString();
				}
			}
		}
		if (update)
			this._updateItem($item, show);
	},
	clickArrayRemove: function(e) {
		var $target = $(e.currentTarget),
			$item = $target.closest('.app-settings-item'),
			value = $target.closest('div').text(),
			arr = this.fields[$item.data('name')];
		arr.splice(_.indexOf(arr, value), 1);
		this._updateItem($item, true);
	},
	submitForm: function(e) {
		e.preventDefault();
		var $target = $(e.currentTarget);
		$target.find('.el-ok').trigger('click');
		$target.find('.el-add').trigger('click');
	}
});
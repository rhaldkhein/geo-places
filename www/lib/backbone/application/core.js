(function($) {
	"use strict";

	if (_.isUndefined(window.Backbone))
		throw new Error('Backbone is required. Version <= v1.1.2');

	// Class that represents the window
	var ViewPort = function() {
		this._px = 'px';
		this._percent = '%';
		var resize = function() {
			var $window = $(window),
				height = $window.height(),
				width = $window.width();
			this.height = height;
			this.width = width;
			if (height > width) {
				this.max = height;
				this.min = width;
				this.portrait = true;
				this.landscape = false;
			} else {
				this.max = width;
				this.min = height;
				this.portrait = false;
				this.landscape = true;
			}
		}.bind(this);
		this.pxMax = function(val, noPx) {
			return this.ptMax(val) + (noPx ? '' : this._px);
		};
		this.pxMin = function(val, noPx) {
			return this.ptMin(val) + (noPx ? '' : this._px);
		};
		this.pxHeight = function(val, noPx) {
			return this.ptHeight(val) + (noPx ? '' : this._px);
		};
		this.pxWidth = function(val, noPx) {
			return this.ptWidth(val) + (noPx ? '' : this._px);
		};
		this.ptMax = function(val) {
			return Math.round(this.max * val);
		};
		this.ptMin = function(val) {
			return Math.round(this.min * val);
		};
		this.ptHeight = function(val) {
			return Math.round(this.height * val);
		};
		this.ptWidth = function(val) {
			return Math.round(this.width * val);
		};
		this.pcWidth = function(px, w, r, s) {
			w = w || this.width;
			px = r ? w - px : px;
			return Math.round((px / w) * 100);
		};
		this.pcHeight = function(px, h, r) {
			h = h || this.height;
			px = r ? h - px : px;
			return Math.round((px / h) * 100);
		};
		// Listen to window resize
		$(window).on('resize', resize);
		resize();
	};

	// Application
	var Application = function() {

		var _lastHash = null;

		// Default options
		this.options = {
			autoRender: true,
			urlPrefix: '',
			urlSuffix: '',
			templateCompiler: null
		};

		// Object namespaces
		this.Model = {};
		this.Collection = {};
		this.View = {};
		this.Data = {};

		// Hash callback storage
		this._hash = [];

		// Cache storage
		this._templates = {};

		// Function that compile template
		this.compiler = null;

		// Callback for out of bounds clicks
		this._cbOutOfBounds = function(e) {
			var self = this,
				target = $(e.target);
			_.each(this._cbOutOfBounds.nonOut, function($el) {
				if ($el.is(e.target) || $el.has(e.target).length) {} else {
					_lastHash = self._hash[self._hash.length - 1];
					if (_lastHash && !_lastHash.$el.is(e.target) && _lastHash.$el.has(e.target).length === 0) {
						history.back();
					}
				}
			});
		}.bind(this);

		// Array of non out of bounds elements
		this._cbOutOfBounds.nonOut = [];

		// Setup a default template compiler, which is underscore's
		if (this.options.templateCompiler === null) {
			this.options.templateCompiler = function(template, data) {
				var compile = _.template(template);
				return compile(data);
			};
		}

	};

	// Application methods
	_.extend(Application.prototype, Backbone.Events, {

		// Attach the viewport to application's prototype
		viewport: new ViewPort(),

		// Function that converts arguments to array
		argsArray: Array.prototype.slice,

		// Cofiguration and export the application
		configure: function(config) {
			_.extend(this.options, config || {});
			return this;
		},

		// Pre initialize function
		_pre_initialize: function() {
			var self = this,
				doc = $(document);
			// Reference local storage
			this.Data = localStorage || {};
			// Models
			_.each(this.Model, function(model, key) {
				model.prototype.url = self.options.urlPrefix + model.prototype.url + self.options.urlSuffix;
			});
			// Collections
			_.each(this.Collection, function(collection, key) {
				collection.prototype.url = self.options.urlPrefix + collection.prototype.url + self.options.urlSuffix;
			});
			// Listen to hashchange
			history.fromApp = false;
			$(window).on('hashchange', function(e) {
				e.preventDefault();
				if (location.hash === '') {
					doc.off('mouseup', this._cbOutOfBounds);
					if (this._hash.length > 0) {
						if (!history.fromApp)
							this._hash.pop()._back();
						if (this._hash.length > 0) {
							location.hash = 'back';
						}
					}
				} else {
					doc.on('mouseup', this._cbOutOfBounds);
				}
				history.fromApp = false;
			}.bind(this));
		},

		// Boot up the application
		_initialize: function() {
			// Templates
			var self = this,
				element = null,
				fnName = null;
			// Get predegined templates
			$('script[type="text/template"]').each(function() {
				element = $(this);
				self._templates[element.attr('id')] = element.html().replace(/[\f\n\r\t\v]*/g, "");
				element.remove();
			});
			// Views
			_.each(this.View, function(view, key) {
				// Enable case insensitive
				fnName = key.toLowerCase();
				// Set the template
				view.prototype.template = fnName;
				// Set the name
				view.prototype.name = key;
				// Expose parent object
				view.prototype.super = view.__super__;
			});
		},

		// Start the application, also starts the Backbone's history
		start: function() {
			var self = this,
				proceed = function(cont) {
					if (cont === false) {
						// Trigger start error
						self.trigger('error');
					} else {
						// Initialize application object
						self._initialize();
						// Set template compiler
						self.compiler = self.options.templateCompiler;
						// Trigger started callback
						self.trigger('started');
					}
				};
			if (this._events && this._events['starting']) {
				// Call pre init handler
				self._pre_initialize();
				// Trigger starting callback for custom initialization
				this.trigger('starting', proceed);
			} else {
				proceed(true);
			}
		},

		// Add template to cache
		addTemplate: function(id, template, override) {
			if (this._templates[id] === undefined) {
				this._templates[id] = template.replace(/[\f\n\r\t\v]*/g, "");
			} else {
				if (override)
					this._templates[id] = template.replace(/[\f\n\r\t\v]*/g, "");
			}
		},

		// Get a template by element id
		getTemplate: function(id) {
			if (id) {
				if (this._templates[id] === undefined) {
					var element = $('script[type="text/template"]#' + id);
					var template = element.html();
					element.remove();
					this._templates[id] = template;
				}
				return this._templates[id];
			}
		},

		// Compile the template
		compile: function(name, data) {
			return this.compiler(this.getTemplate(name), data);
		},

		// Check if object is a model or a collection
		isModel: function(obj) {
			if (obj instanceof Backbone.Model)
				return 1;
			else if (obj instanceof Backbone.Collection)
				return 2;
			return 0;
		},

		// Get or set data storage
		data: function(key, val) {
			if (val) {
				this.Data[key] = val;
				return this;
			} else if (key) {
				return this.Data[key];
			} else {
				return this.Data;
			}
		},

		// Set a non out of bounds element
		nonOutOfBounds: function(el) {
			if (_.indexOf(this._cbOutOfBounds.nonOut, el) === -1) {
				this._cbOutOfBounds.nonOut.push(el);
			}
		},

		// Hide all panels
		hideAllPanels: function() {
			_.each(this._hash, function(view) {
				nextTick(function() {
					view.hide();
				});
			});
		}

	});

	// Expose application constructor
	Backbone.Application = Application;

	// Instantiate default application
	window.App = new Application();

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

	Backbone.Model.prototype.initialize = Backbone.Collection.prototype.initialize = function() {
		this.view = null;
		if (this.init)
			this.init.apply(this, arguments);
	};

	// Backbone.View Extension
	// ------------------------

	Backbone.View.options = ['autoRender', 'persist', 'template', 'route', 'data', 'models'];

	// Extend Backbone's View prototype
	_.extend(Backbone.View.prototype, {
		// Parent view of this view
		parent: null,
		// Data for template
		data: {},
		// The route responsibly for a view
		route: null,
		// Mark to display error template
		_error: false,
		// Data for error template
		_errorData: null,
		// Template for error
		_errorTemplate: null,
		// Default compile option
		compile: true,
		// Set a variable or model for template
		set: function(name, value, listen) {
			listen = listen || false;
			if (_.isObject(name)) {
				if (_.isBoolean(value))
					listen = value;
				_.each(name, function(val, id) {
					if (App.isModel(val) !== 0) {
						this.models[id] = val;
						if (listen)
							this.listen(val);
					} else {
						this.data[id] = val;
					}
				}, this);
			} else {
				if (App.isModel(value) !== 0) {
					this.models[name] = value;
					if (listen)
						this.listen(value);
				} else {
					this.data[name] = value;
				}
			}
		},
		// Override view's initialize for us to auto trigger the render
		initialize: function() {
			// If style is present, hide the element using visibility so that width and height will be usable
			if (this.style)
				this.el.style.visibility = 'hidden';
			// Need to hide this view first if controlBack is enabled
			if (this.controlBack)
				this.hide();
			// Configure this view
			this._config();
			// Generate selector
			this.selector = this.tagName + (this.el.id ? ('#' + this.el.id) : '') + (this.className ? ('.' + this.className.replace(/ /g, '.')) : '') + ' ';
			// Prepare a handler for back button
			if (this.controlBack)
				this._back = this._back.bind(this);
			// List of models and collections
			this.models = {};
			// Set to true when a view is rendered
			this.isRendered = false;
			// Set to true when a view is initialized
			this.isDone = false;
			// Trigger external init function
			if (this.init)
				this.init.apply(this, App.argsArray.call(arguments));
			// Trigger filters
			this._runFilters();
			// Trigger if initialized
			if (!this.isDone && this.done) {
				this.isDone = true;
				this.done.call(this);
			}
		},
		// Run all filters (entry point for cached views)
		_runFilters: function() {
			if (this.filters && !this._error) {
				_.each(this.filters, function(fn) {
					if (!this._error)
						App.Filter[fn].call(this);
				}, this);
			}
			if (this.filter && !this._error) {
				this.filter.call(this);
			}
			if (this.autoRender !== false && App.options.autoRender) {
				this.data.LOADED = false;
				this.render();
			}
		},
		// Renders a View that require some properties from context
		render: function(tpl, data) {
			if (App.options.debug)
				console.info('Rendering View:', this);
			// Predefine data loaded
			this.data.LOADED = this.data.LOADED || false;
			// Predefine data for template
			if (this.route) {
				this.data.ROUTE = this.route.route;
				this.data.QUERY = this.route.query;
				this.data.QUERYSTRING = _.isEmpty(this.data.QUERY) ? '' : $.param(this.data.QUERY);
			}
			// Get the template for 
			tpl = tpl || App.getTemplate(this.template);
			// Loop up parent's template if child's is undefined
			var modelData = {}, currentContext = this;
			while (currentContext.template && tpl === undefined) {
				tpl = App.getTemplate(currentContext.template);
				currentContext = currentContext.constructor.__super__;
			}
			// Extract data for template
			_.each(this.models, function(value, key) {
				if (value instanceof Backbone.Model || value instanceof Backbone.Collection) {
					modelData[key] = value.toJSON();
				}
			});
			// If persist, do not recompile, just re-show the element
			if (this.persist && this.isRendered) {
				this.$el.show();
			} else {
				// Compile template
				this.$el.html(this.compile ? App.compiler(tpl || this.$el.html(), _.extend({}, this.data, modelData, data)) : tpl);
			}
			// Paste into the outlet element
			if (this.outlet) {
				$(this.outlet).html(this._error ? App.compiler(App.getTemplate(this._errorTemplate), this._errorData) : this.el);
			}
			// Re-delegate events
			this.delegateEvents();
			// Parse style if not
			this._parseStyle();
			// Flag as rendered
			this.isRendered = true;
			// Trigger rendered callback
			if (this.rendered)
				this.rendered.call(this);
			// Return this object for chaining
			return this;
		},
		placeIn: function(view) {
			if (view.isRendered) {
				this.$el.html(view.el);
				view._parseStyle();
				view.parent = this;
			}
			return this;
		},
		placeInChild: function(view, element) {
			if (view.isRendered) {
				this.$el.find(element).html(view.el);
				view._parseStyle();
				view.parent = this;
			}
			return this;
		},
		append: function(view, element) {
			if (view.isRendered) {
				var el = element ? this.$el.find(element) : this.$el;
				el.append(view.el);
				view._parseStyle();
				view.parent = this;
			}
			return this;
		},
		prepend: function(view, element) {
			if (view.isRendered) {
				var el = element ? this.$el.find(element) : this.$el;
				el.prepend(view.el);
				view._parseStyle();
				view.parent = this;
			}
			return this;
		},
		listen: function(model) {
			if (model instanceof Backbone.Model) {
				model.view = this;
				if (App.options.autoRender) {
					model.on('change', function(e) {
						if (App.options.debug)
							console.info('Model Changed:', e);
						this.data.LOADED = true;
						if (this.autoRender !== false)
							this.render();
					}, this);
				}
			} else if (model instanceof Backbone.Collection) {
				model.view = this;
				if (App.options.autoRender) {
					var fn = function(e) {
						if (App.options.debug)
							console.info('Collection Changed:', e);
						this.data.LOADED = true;
						if (this.autoRender !== false)
							this.render();
					};
					model.on('sync', fn, this).on('add', fn, this).on('remove', fn, this);
				}
			}
		},
		show: function() {
			var self = this;
			setTimeout(function() {
				self.visible(true);
			}, 0);
			return this;
		},
		hide: function() {
			var self = this;
			setTimeout(function() {
				self.visible(false);
			}, 0);
			return this;
		},
		visible: function(bool, silent) {
			if (bool === false) {
				// Hide element
				this.el.style.display = 'none';
				// Trigger hide hook
				this.trigger('hide');
				// Control with back button, remove the handler
				if (silent) {
					this.trigger('back');
				} else {
					if (this.controlBack) {
						var self = this;
						if (App._hash.length > 0 && App._hash[App._hash.length - 1] === this) {
							history.fromApp = true;
							history.back();
						}
						_.remove(App._hash, function(view) {
							return view === self;
						});
					}
				}
			} else {
				// Display element by removing display style
				this.el.style.display = '';
				// Trigger show hook
				this.trigger('show');
				// Re-delegate events
				this.delegateEvents();
				// Control with back button, add the handler
				if (this.controlBack) {
					var lastHash = App._hash[App._hash.length - 1];
					if (lastHash && lastHash.cid === this.cid) {
						// Do nothing...
					} else {
						_.each(App._hash, function(view) {
							view.el.style.zIndex = 0;
						});
						this.el.style.zIndex = 1000;
						App._hash.push(this);
						location.hash = 'back';
					}
				}
				// Try parsing the style in case it has 
			}
			return this;
		},
		isVisible: function() {
			return this.$el.is(':visible') || false;
		},
		error: function(data, name) {
			this._error = true;
			this._errorData = data || {};
			this._errorTemplate = name || 'error';
			this.render();
			// Revert back the mark
			this._error = false;
		},
		_parseStyle: function() {
			// Parse style if specified
			// Check if this view is attached to DOM by checking its parents  && this.$el.parent().length
			if (this.style && this.$el.parent().length) {
				var self = this;
				this.style(this._addStyle.bind(this), App.viewport);
				this.style = false;
				// If style is parse then we can show the element
				setTimeout(function() {
					// self.$el.css('visibility', 'visible');
					self.el.style.visibility = 'visible';
				}, 0);
			}
		},
		_addStyle: function(element, style, value, inside) {
			var $el = $('style#css-' + this.template),
				res = '',
				obj = null,
				iA = null,
				iB = null;
			if (!_.isPlainObject(element)) {
				obj = {};
				if (_.isPlainObject(style)) {
					obj[element] = style;
				} else {
					obj[element] = {};
					obj[element][style] = value;
				}
			} else {
				obj = element;
			}
			inside = _.isUndefined(inside);
			for (iA in obj) {
				res += (inside ? this.selector : '') + iA + '{';
				for (iB in obj[iA]) {
					res += iB + ':' + obj[iA][iB] + ';';
				}
				res += '} ';
			}
			if ($el.length > 0) {
				// Exisitng style is present, replace it
				$el.append(res);
			} else {
				// Create style element and append it to document
				$('<style/>').attr({
					id: 'css-' + this.template,
					type: 'text/css',
					rel: 'stylesheet'
				}).text(res).appendTo($('head'));
			}
		},
		_back: function() {
			this.visible(false, true);
		},
		_config: function() {
			this.options = this.options || {};
			_.extend(this, _.pick(this.options, this.constructor.options));
			_.extend(this.data, {
				LOADED: false,
				ROUTE: undefined,
				QUERY: undefined,
				QUERYSTRING: undefined
			});
		}
	});

})(jQuery);
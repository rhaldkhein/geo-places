/**
 * List control for application framework.
 */

/**
 * Inject the template for list, this can be overridden by script
 * tag in html / document file or child's template.
 *
 * Template requires app-list-container class
 */
App.addTemplate('list', '<ul class="app-list-container"></ul>');

/**
 * List control class
 */
App.View.List = Backbone.View.extend({
	className: 'app-list',
	events: {
		'click li.app-list-item': 'clickItem'
	},
	$listContainer: null,
	$itemWrapper: $('<li/>'),
	init: function(opt) {
		opt = opt || {};
		// Add class if overridden
		this.$el.addClass('app-list');
		// Stick options for future use
		this.options = opt;
	},
	done: function() {
		var self = this;
		// Ensure list has container
		this.$listContainer = this.$el.find('.app-list-container');
		if (!this.$listContainer.length) {
			this.$listContainer = this.$el;
		}
		// Add predefined items
		if (this.options.items) {
			$.each(this.options.items, function(i, v) {
				self.addItem(v);
			});
		}
	},
	clickItem: function(e) {
		this.trigger('itemclick', {
			index: $(e.currentTarget).index(),
			item: e.currentTarget
		}, e);
	},
	addItem: function(item, group) {
		this.$listContainer.append(this.$itemWrapper.clone().addClass('app-list-item').html(item));
		this.trigger('itemadd', {
			item: item
		});
	},
	// addGroup: function(label) {
	// 	this.$listContainer.append(this.$itemWrapper.clone().addClass('app-list-item-group').html(item));
	// },
	removeItem: function(index) {
		var item = this.$listContainer.children().eq(index);
		this.trigger('itemremove', {
			item: item
		});
		item.remove();
	}
});
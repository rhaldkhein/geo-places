App.View.Head = Backbone.View.extend({
	el: '#gx-head',
	events: {
		'click #gx-head-btn-online': 'clickOnline',
		'click #gx-head-btn-main': 'clickMain'
	},
	clickMain: function() {
		HandLeft.toggle('game');
	},
	clickOnline: function() {
		HandRight.toggle('online');
	},
	style: function(css, win) {
		var headHeight = 0.07,
			headFontSize = 0.028;
		css('', 'height', win.pxMax(headHeight));
		css('.ui-menu-bar button', 'font-size', win.pxMax(headFontSize));
		// Apply css globally by providing true argument
		css('.ui-screen', 'top', win.pxMax(headHeight), true);
	},
	init: function() {
		// Show top bar header
		this.show();
		// Bind update callback
		this.update = this.update.bind(this);
		// Set this view as non out bounds
		App.nonOutOfBounds(this.$el);
		// Update the title text
		this.title(User.get('id'));
		// Listen to online change event
		Online.on('reset', this.update).on('add', this.update).on('remove', this.update);
	},
	done: function() {
		setTimeout(function() {
			this.$el.find('#gx-head-btn-online').click();
		}.bind(this));
	},
	update: function() {
		this.$el.find('.gx-online-count').text(Online.length);
	},
	title: function(str) {
		$('#gx-head-btn-main').text(str);
	}
});